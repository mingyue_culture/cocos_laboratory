/**
 * map
 * date: 2018.07.12
 * author: lingfeng
 */

/**
 * 高性能map，key类型只支持 string|number|symbol
 * 因object中，string索引和number索引的结果一样，
 * object无法重写取值赋值器
 * 故不做泛型约定
 */
export class KMap {
    constructor() {
    }

    public copy():KMap{
        let newmap = new KMap();
        for (var key of this.keys){
            newmap[key] = this[key];
        }
        return newmap;
    }

    public get length():number {
        return this.keys.length;
    }

    public get keys():string[]{
        return Object.keys(this);
    }

    public remove(key:string){
        delete this[key];
    }

    public clear(){
        for(var key in this){
            this.remove(key);
        }
    }
}


/**
 * 通用map，list存储，性能低，key，value类型不限。
 * 不建议使用：key类型为string|int|symbol，且键值对量级大
 */
export default class MKMap {
    private elements:Array<any>;

    public constructor() {
        this.elements = new Array();
    }
    
    //获取Map元素个数
    public size() {
        return this.elements.length;
    }

    //判断Map是否为空
    public isEmpty() {
        return (this.elements.length < 1);
    }

    //删除Map所有元素
    public clear() {
        this.elements = new Array();
    }

    //向Map中增加元素（key, value)
    public put(_key, _value) {
        if (this.containsKey(_key) == true) {
            if(this.remove(_key) == true){
                this.elements.push( {
                    key : _key,
                    value : _value
                });
            }
        } else {
            this.elements.push( {
                key : _key,
                value : _value
            });
        }
    }

    //删除指定key的元素，成功返回true，失败返回false
    public remove(_key) {
        var bln = false;
        try {
            for (var i = 0; i < this.elements.length; i++) {
                if (this.elements[i].key == _key){
                    this.elements.splice(i, 1);
                    return true;
                }
            }
        }catch(e){
            bln = false;
        }
        return bln;
    }

    //获取指定key的元素值value，失败返回null
    public get(_key) {
        try{
            for (var i = 0; i < this.elements.length; i++) {
                if (this.elements[i].key == _key) {
                    return this.elements[i].value;
                }
            }
        }catch(e) {
            return null;
        }

        return null;
    }

    //获取指定索引的元素（使用element.key，element.value获取key和value），失败返回null
    public element(_index) {
        if (_index < 0 || _index >= this.elements.length){
            return null;
        }
        return this.elements[_index];
    }

    //判断Map中是否含有指定key的元素
    public containsKey(_key) {
        var bln = false;
        try {
            for (var i = 0; i < this.elements.length; i++) {
                if (this.elements[i].key == _key){
                    bln = true;
                }
            }
        }catch(e) {
            bln = false;
        }
        return bln;
    }

    //判断Map中是否含有指定value的元素
    public containsValue(_value) {
        var bln = false;
        try {
            for (var i = 0; i < this.elements.length; i++) {
                if (this.elements[i].value == _value){
                    bln = true;
                }
            }
        }catch(e) {
            bln = false;
        }
        return bln;
    }

    //获取Map中所有key的数组（array）
    public keys() {
        var arr = new Array();
        for (var i = 0; i < this.elements.length; i++) {
            arr.push(this.elements[i].key);
        }
        return arr;
    }

    //获取Map中所有value的数组（array）
    public values() {
        var arr = new Array();
        for (var i = 0; i < this.elements.length; i++) {
            arr.push(this.elements[i].value);
        }
        return arr;
    }

    public sort(cmd?) {
        if(cmd == undefined){
            this.elements.sort();
        }
        else{
            this.elements.sort(cmd);
        }

        return this.elements;
    }

    public getFistElement(){
        this.elements.shift();
    }
}