/**
 * 二进制包对应加解密包函数体
 */

export default class NetProto {
    public constructor() {

    }

    public static S_heart_beat = function () {
        return {
            F_id: 0,
            Pack: function (writer) {
                writer.writeS32(this.F_id);
            },
            Unpack: function (reader) {
                this.F_id = reader.readS32();
            }
        }
    };

    public static S_user_login_info_req = function () {
        return {
            F_code: "",
            F_channel: "",
            F_source: "",
            F_openid: "",
            F_token: "",
            Pack: function (writer) {
                writer.writeString(this.F_code);
                writer.writeString(this.F_channel);
                writer.writeString(this.F_source);
                writer.writeString(this.F_openid);
                writer.writeString(this.F_token);
            },
            Unpack: function (reader) {
                this.F_code = reader.readString();
                this.F_channel = reader.readString();
                this.F_source = reader.readString();
                this.F_openid = reader.readString();
                this.F_token = reader.readString();
            }
        }
    };

    public static S_user_login_info_ack = function () {
        return {
            F_uid: 0,
            F_server_id: 0,
            F_server_list: 0,
            F_anounc_list: 0,
            F_cloud_list: 0,
            Pack: function (writer) {
                writer.writeI64(this.F_uid);
                writer.writeI64(this.F_server_id);
                writer.writeString(this.F_server_list);
                writer.writeString(this.F_anounc_list);
                writer.writeString(this.F_cloud_list);
            },
            Unpack: function (reader) {
                this.F_uid = reader.readI64();
                this.F_server_id = reader.readI64();
                this.F_server_list = reader.readString();
                this.F_anounc_list = reader.readString();
                this.F_cloud_list = reader.readString();
            }
        }
    };

    public static S_re_conn_req = function () {
        return {
            F_uid: 0,
            F_server_id: 0,
            Pack: function (writer) {
                writer.writeI64(this.F_uid);
                writer.writeI64(this.F_server_id);
            },
            Unpack: function (reader) {
                this.F_uid = reader.readI64();
                this.F_server_id = reader.readI64();
            }
        }
    };

    public static S_re_conn_ack = function () {
        return {
            F_success: false,
            Pack: function (writer) {
                writer.writeBool(this.F_success);
            },
            Unpack: function (reader) {
                this.F_success = reader.readBool();
            }
        }
    }

    public static S_choose_game_server_req = function () {
        return {
            F_id: 0,
            Pack: function (writer) {
                writer.writeI64(this.F_id);
            },
            Unpack: function (reader) {
                this.F_id = reader.readI64();
            }
        }
    };

    public static S_choose_game_server_ack = function () {
        return {
            F_is_new_user: false,
            F_guide: 0,
            Pack: function (writer) {
                writer.writeBool(this.F_is_new_user);
                writer.writeI64(this.F_guide);
            },
            Unpack: function (reader) {
                this.F_is_new_user = reader.readBool();
                this.F_guide = reader.readI64();
            }
        }
    };

    public static S_seed_info = function () {
        return {
            F_client_send_seed: 0,
            F_client_receive_seed: 0,
            Pack: function (writer) {
                writer.writeS32(this.F_client_send_seed);
                writer.writeS32(this.F_client_receive_seed);
            },
            Unpack: function (reader) {
                this.F_client_send_seed = reader.readS32();
                this.F_client_receive_seed = reader.readS32();
            }
        }
    };

    public static S_choose_game_info = function () {
        return {
            F_game_name: "",
            F_load_param: 0,
            Pack: function (writer) {
                writer.writeString(this.F_game_name);
                writer.writeS32(this.F_load_param);
            },
            Unpack: function (reader) {
                this.F_game_name = reader.readString();
                this.F_load_param = reader.readS32();
            }
        }
    };

    public static S_error_info = function () {
        return {
            F_code: 0,
            F_msg: "",
            F_show: "",
            Pack: function (writer) {
                writer.writeS32(this.F_code);
                writer.writeString(this.F_msg);
                writer.writeString(this.F_show);
            },
            Unpack: function (reader) {
                this.F_code = reader.readS32();
                this.F_msg = reader.readString();
                this.F_show = reader.readString();
            }
        }
    };

    public static S_game_data_bytes = function () {
        return {
            F_msg_type: 0,  //默认值为0
            F_msg_id: 0,     //默认值为0，客户端传送什么，原封不动返回。
            F_msg: new ArrayBuffer(0),
            Pack: function (writer) {
                writer.writeS32(this.F_msg_type);
                writer.writeS32(this.F_msg_id);
                writer.writeBuffer(this.F_msg);
            },
            Unpack: function (reader) {
                this.F_msg_type = reader.readS32();
                this.F_msg_id = reader.readS32();
                this.F_msg = reader.readBuffer();
            }
        }
    };
}
