/**
 * Author: ling_feng
 * Date: 2018.5.30
 * CopyRight:
 * Proto 文件解析管理
 */

import * as game from "./game.js"

export var proto: any;

export default class ProtoManager {
    private static ins: ProtoManager;
    public static get I(): ProtoManager {
        if (!ProtoManager.ins) {
            ProtoManager.ins = new ProtoManager();
        }
        return ProtoManager.ins;
    }

    public constructor() {

    }

    public initCommon() {
        //cc.log("[ProtoManager] initCommon");

        if (!proto) {
            proto = game["protos"];
        }
    }
}