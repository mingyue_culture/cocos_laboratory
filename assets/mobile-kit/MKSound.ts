import MKLog from "./MKLog";

/**
 * Author: ling_feng
 * Date: 2018.5.16
 * CopyRight:
 * 音效管理类
 */

export const isEffectOn: boolean = false;
export const isMusicOn: boolean = false;

export default class MKSound {
    private static bgmVolume: number = 1.0;
    private static sfxVolume: number = 1.0;
    private static bgmAudioID: number = -1;
    private static sfxAudioID: number = -1;

    public constructor() {
        cc.game.on(cc.game.EVENT_HIDE, function () {
            MKLog.log("cc.audioEngine.pauseAll");
            cc.audioEngine.pauseAll();
        });

        cc.game.on(cc.game.EVENT_SHOW, function () {
            MKLog.log("cc.audioEngine.resumeAll");
            cc.audioEngine.resumeAll();
        });
    }

    /**
     * 转化地址
     * @param url 
     */
    private static getUrl(url) {
        return cc.url.raw("resources/sounds/" + url);
    }

    /**
     * 播放音乐
     * @param url 
     */
    public static playBGM(url: string) {
        if (!isMusicOn) {
            return;
        }

        if (this.bgmAudioID >= 0) {
            cc.audioEngine.stop(this.bgmAudioID);
        }

        var audioUrl = this.getUrl(url);
        MKLog.log("[MKSound] playBGM = ", audioUrl);
        this.bgmAudioID = cc.audioEngine.play(audioUrl, true, this.bgmVolume);
    }

    /**
     * 播放音效
     * @param url 
     */
    public static playSFX(url: string) {
        if (!isEffectOn) {
            return;
        }

        if (this.sfxVolume >= 0) {
            cc.audioEngine.stop(this.sfxAudioID);
        }

        let audioUrl = this.getUrl(url);
        MKLog.log("playSFXplaySFXplaySFXplaySFX =", audioUrl);
        if (audioUrl && this.sfxVolume > 0) {
            var audioId = cc.audioEngine.play(audioUrl, false, this.sfxVolume);

            MKLog.log("[MKSound] playSFX = ", audioId);
            return audioId;
        }

        return 0;
    }

    public static setSFXVolume(v) {
        if (this.sfxVolume != v) {
            cc.sys.localStorage.setItem("sfxVolume", v);
            this.sfxVolume = v;
        }
    }

    public static setBGMVolume(v, force) {
        if (this.bgmAudioID >= 0) {
            if (v > 0) {
                cc.audioEngine.resume(this.bgmAudioID);
            }
            else {
                cc.audioEngine.pause(this.bgmAudioID);
            }
            //cc.audioEngine.setVolume(this.bgmAudioID,this.bgmVolume);
        }
        if (this.bgmVolume != v || force) {
            cc.sys.localStorage.setItem("bgmVolume", v);
            this.bgmVolume = v;
            cc.audioEngine.setVolume(this.bgmAudioID, v);
        }
    }

    public static pauseAll() {
        cc.audioEngine.pauseAll();
    }

    public static resumeAll() {
        cc.audioEngine.resumeAll();
    }

    public static stopAll() {
        cc.audioEngine.stopAll();
    }

    public static stopAppointAudio(id: number) {
        cc.audioEngine.stop(id);
    }

    public static stopBGM(): void {
        if (this.bgmAudioID >= 0) {
            cc.audioEngine.stop(this.bgmAudioID);
        }
    }
}