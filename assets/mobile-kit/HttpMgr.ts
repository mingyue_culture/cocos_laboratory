import MKLog from "./MKLog";

/**
 * Author: ling_feng
 * Date: 2018.5.16
 * CopyRight:
 * Http协议通信类
 */

let haveEncode: boolean = false;
let retryTime: number = 0;
export function httpGet(url, callback, isText: boolean = false, timeout: number = 6000, retryCount: number = 3) {
    var self = this;
    if (!haveEncode) {
        haveEncode = true;
        url = encodeURI(url);
    }
    //cc.log("get:" + url);
    // xr.net.showLoading();
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        MKLog.log("get status:" + xhr.status);
        if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
            haveEncode = false;
            var response = xhr.responseText;
            retryTime = 0;
            //是否返回纯文本
            if (isText) {
                callback(0, response);
                return;
            }
            MKLog.log(response);
            try {
                response = JSON.parse(response);
            } catch (e) {
                MKLog.log("get:" + JSON.stringify(e));
                response = null;
                callback("解析结果失败", response);
                return;
            }
            callback(0, response);
        }
    };

    xhr.onerror = function (e: ErrorEvent) {
        //cc.log(e.message);
        callback("返回错误");
        //callback(e.errorBuffer);
        //xr.scene.showMsgBox(e.errorBuffer);
        retryTime = 0;
        haveEncode = false;
    };

    xhr.timeout = timeout;
    //cc.log('超时时间:' + xhr.timeout);
    xhr.ontimeout = function (e) {
        if (retryTime >= retryCount) {
            callback("访问超时");
            retryTime = 0;
            haveEncode = false;
        } else {
            retryTime++;
            //callback("访问超时重试:" + retryTime);
            httpGet(url, callback, isText, timeout, retryCount);
        }
    };
    //  xhr.overrideMimeType();
    xhr.open("GET", url, true);
    xhr.send();
}

export function httpPost(url, callback, isText: boolean = false, timeout: number = 6000, retryCount: number = 3, data: string) {
    var self = this;
    if (!haveEncode) {
        haveEncode = true;
        url = encodeURI(url);
    }
    //cc.log("post:" + url);
    // xr.net.showLoading();
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        //MKLog.log("post status:"+xhr.status);
        if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
            haveEncode = false;
            var response = xhr.responseText;
            retryTime = 0;
            //是否返回纯文本
            if (isText) {
                callback(0, response);
                return;
            }
            MKLog.log(response);
            try {
                response = JSON.parse(response);
            } catch (e) {
                // MKLog.log("post:"+JSON.stringify(e));
                response = null;
                callback("解析结果失败", response);
                return;
            }
            callback(0, response);
        }
    };

    xhr.onerror = function (e: ErrorEvent) {
        //cc.log(e.message);
        callback("返回错误");
        //callback(e.errorBuffer);
        //xr.scene.showMsgBox(e.errorBuffer);
        retryTime = 0;
        haveEncode = false;
    };

    xhr.timeout = timeout;
    //cc.log('超时时间:' + xhr.timeout);
    xhr.ontimeout = function (e) {
        if (retryTime >= retryCount) {
            callback("访问超时");
            retryTime = 0;
            haveEncode = false;
        } else {
            retryTime++;
            //callback("访问超时重试:" + retryTime);
            httpGet(url, callback, isText, timeout, retryCount);
        }
    };
    //  xhr.overrideMimeType();
    xhr.open("POST", url, true);
    xhr.send(data);
}
