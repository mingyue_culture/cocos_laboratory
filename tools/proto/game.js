module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "protos",
    "syntax": "proto2",
    "messages": [
        {
            "name": "SyncUserInfoReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "source",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "platform",
                    "id": 2
                }
            ]
        },
        {
            "name": "SyncUserInfoRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "user_status",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "uid",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "nickname",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "avatar",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "change",
                    "id": 5,
                    "options": {
                        "packed": true
                    }
                }
            ]
        },
        {
            "name": "SyncUserSquadReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "index",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "name",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "is_main",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "squad",
                    "id": 4,
                    "options": {
                        "packed": true
                    }
                }
            ]
        },
        {
            "name": "UserHero",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "hero_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint64",
                    "name": "fp",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint64",
                    "name": "level",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint64",
                    "name": "exp",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "star_level",
                    "id": 5
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "skills",
                    "id": 6,
                    "options": {
                        "packed": true
                    }
                },
                {
                    "rule": "map",
                    "type": "int64",
                    "keytype": "int32",
                    "name": "fight",
                    "id": 7
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "equipment",
                    "id": 8,
                    "options": {
                        "packed": true
                    }
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "on_squad",
                    "id": 9
                }
            ]
        },
        {
            "name": "SquadShow",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "HeroShow",
                    "name": "heros_show",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "HeroShow",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "hero_id",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "uint64",
                            "name": "level",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "star_level",
                            "id": 3
                        },
                        {
                            "rule": "repeated",
                            "type": "Equipment",
                            "name": "equipment",
                            "id": 4
                        }
                    ]
                }
            ]
        },
        {
            "name": "UserHeroListRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "UserHero",
                    "name": "list",
                    "id": 1
                }
            ]
        },
        {
            "name": "Fight",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "id",
                    "id": 1
                },
                {
                    "rule": "map",
                    "type": "int64",
                    "keytype": "int32",
                    "name": "fight",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "skills",
                    "id": 3,
                    "options": {
                        "packed": true
                    }
                }
            ]
        },
        {
            "name": "DungeonStateRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "DungeonState",
                    "keytype": "int32",
                    "name": "dungeon_state",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "DungeonState",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "progress",
                            "id": 1
                        },
                        {
                            "rule": "map",
                            "type": "ChapterDetail",
                            "keytype": "int32",
                            "name": "detail",
                            "id": 2
                        }
                    ],
                    "messages": [
                        {
                            "name": "ChapterDetail",
                            "syntax": "proto3",
                            "fields": [
                                {
                                    "rule": "repeated",
                                    "type": "DungeonPassStar",
                                    "name": "star",
                                    "id": 1,
                                    "options": {
                                        "packed": true
                                    }
                                },
                                {
                                    "rule": "optional",
                                    "type": "int32",
                                    "name": "box",
                                    "id": 2
                                },
                                {
                                    "rule": "repeated",
                                    "type": "int32",
                                    "name": "challenges",
                                    "id": 3,
                                    "options": {
                                        "packed": true
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "name": "TowerState",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "history",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "progress",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "sweep_surplus_time",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "reset_times",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "is_box_free",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "box_num",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "open_index",
                    "id": 7
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "pass_reward",
                    "id": 8
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "box_reward",
                    "id": 9
                }
            ]
        },
        {
            "name": "TowerFightBeginReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "tower_id",
                    "id": 2
                }
            ]
        },
        {
            "name": "TowerFightBeginRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Fight",
                    "name": "heros_info",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Fight",
                    "name": "enemies_info",
                    "id": 2
                }
            ]
        },
        {
            "name": "TowerFightEndReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "result",
                    "id": 1
                }
            ]
        },
        {
            "name": "TowerFightEndRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "TowerState",
                    "name": "state",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "pass_rewards",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "first_pass_rewards",
                    "id": 3
                }
            ]
        },
        {
            "name": "DungeonFightBeginReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "chapterId",
                    "id": 1
                }
            ]
        },
        {
            "name": "DungeonFightBeginRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Fight",
                    "name": "heros_info",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Fights",
                    "name": "enemies_info",
                    "id": 2
                }
            ],
            "messages": [
                {
                    "name": "Fights",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "repeated",
                            "type": "Fight",
                            "name": "fights",
                            "id": 1
                        }
                    ]
                }
            ]
        },
        {
            "name": "DungeonFightEndReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "DungeonPassStar",
                    "name": "star",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "chapterId",
                    "id": 2
                }
            ]
        },
        {
            "name": "DungeonFightEndRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "player_exp",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "hero_exp",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "rewards",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "ids_exp",
                    "id": 4
                }
            ]
        },
        {
            "name": "ReceivingBoxReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "chapterId",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "boxIndex",
                    "id": 2
                }
            ]
        },
        {
            "name": "ReceivingRewardRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "rewards",
                    "id": 1
                }
            ]
        },
        {
            "name": "DungeonSweepReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "chapterId",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "times",
                    "id": 2
                }
            ]
        },
        {
            "name": "DungeonSweepRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "SweepRewards",
                    "name": "sweep_rewards",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "SweepRewards",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "player_exp",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "hero_exp",
                            "id": 2
                        },
                        {
                            "rule": "repeated",
                            "type": "Item",
                            "name": "rewards",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "QualifyingRefreshRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Competitor",
                    "name": "competitor",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "now_rank",
                    "id": 2
                }
            ],
            "messages": [
                {
                    "name": "Competitor",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "cid",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "name",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "rank",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "uint64",
                            "name": "fp",
                            "id": 4
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "show",
                            "id": 5
                        }
                    ]
                }
            ]
        },
        {
            "name": "QualifyingFightReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "cid",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "rank",
                    "id": 2
                }
            ]
        },
        {
            "name": "QualifyingFightBeginRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "code",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Fight",
                    "name": "heros",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "Fight",
                    "name": "enemy",
                    "id": 3
                }
            ]
        },
        {
            "name": "QualifyingFightEndReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "result",
                    "id": 1
                }
            ]
        },
        {
            "name": "QualifyingFightEndRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "GetQualifyingStateRes",
                    "name": "state",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "reward",
                    "id": 2
                }
            ]
        },
        {
            "name": "GetQualifyingStateRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "History",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "Rank",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint64",
                    "name": "Fp",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "Show",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ChallengeTimes",
                    "id": 5
                }
            ]
        },
        {
            "name": "GetQualifyingRecordRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Record",
                    "name": "record",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "Record",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "bool",
                            "name": "result",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "score",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "name1",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "uint64",
                            "name": "fp1",
                            "id": 4
                        },
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "name2",
                            "id": 5
                        },
                        {
                            "rule": "optional",
                            "type": "uint64",
                            "name": "fp2",
                            "id": 6
                        }
                    ]
                }
            ]
        },
        {
            "name": "AnouncementRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "info",
                    "name": "anoun_list",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "info",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "title",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "anoun_message",
                            "id": 2
                        }
                    ]
                }
            ]
        },
        {
            "name": "UpdateUserInfoReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "source",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "nickname",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "avatar",
                    "id": 3
                }
            ]
        },
        {
            "name": "Item",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "number",
                    "id": 2
                }
            ]
        },
        {
            "name": "GetMailListRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Detail",
                    "name": "MailList",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "Detail",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "mail_id",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "title",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "content",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "Flag",
                            "name": "flag",
                            "id": 4
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "create_time",
                            "id": 6
                        },
                        {
                            "rule": "repeated",
                            "type": "Item",
                            "name": "rewards",
                            "id": 7
                        }
                    ]
                }
            ]
        },
        {
            "name": "MailMarkedReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "mail_ids",
                    "id": 2,
                    "options": {
                        "packed": true
                    }
                },
                {
                    "rule": "optional",
                    "type": "Flag",
                    "name": "flag",
                    "id": 3
                }
            ]
        },
        {
            "name": "MailRewardReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "mail_ids",
                    "id": 2,
                    "options": {
                        "packed": true
                    }
                }
            ]
        },
        {
            "name": "StoreListReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "RefreshType",
                    "name": "refresh_type",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "StoreType",
                    "name": "store_type",
                    "id": 2
                }
            ]
        },
        {
            "name": "StoreListRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "info",
                    "name": "ingots_list",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "info",
                    "name": "rank_list",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "info",
                    "name": "weapon_list",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "info",
                    "name": "vip_list",
                    "id": 4
                }
            ],
            "messages": [
                {
                    "name": "info",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "repeated",
                            "type": "data",
                            "name": "list",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "refresh_id",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "refresh_surplus_num",
                            "id": 3
                        }
                    ],
                    "messages": [
                        {
                            "name": "data",
                            "syntax": "proto3",
                            "fields": [
                                {
                                    "rule": "optional",
                                    "type": "int64",
                                    "name": "id",
                                    "id": 1
                                },
                                {
                                    "rule": "optional",
                                    "type": "int64",
                                    "name": "surplus_time",
                                    "id": 2
                                },
                                {
                                    "rule": "optional",
                                    "type": "int64",
                                    "name": "surplus_num",
                                    "id": 3
                                },
                                {
                                    "rule": "optional",
                                    "type": "bool",
                                    "name": "is_have",
                                    "id": 4
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "name": "BuyGoodsReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "sid",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "num",
                    "id": 2
                }
            ]
        },
        {
            "name": "NewStoreListReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "RefreshType",
                    "name": "refresh_type",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "StoreType",
                    "name": "store_type",
                    "id": 2
                }
            ]
        },
        {
            "name": "NewStoreListRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "store",
                    "name": "store_list",
                    "id": 1
                },
                {
                    "rule": "map",
                    "type": "refresh",
                    "keytype": "int32",
                    "name": "refresh_list",
                    "id": 2
                }
            ],
            "messages": [
                {
                    "name": "store",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "sid",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "surplus_num",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "StoreType",
                            "name": "store_type",
                            "id": 4
                        }
                    ]
                },
                {
                    "name": "refresh",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "free_num",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "other_num",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "surplus_sec",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "extends",
                            "id": 4
                        }
                    ]
                }
            ]
        },
        {
            "name": "NewBuyGoodsReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "sid",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "num",
                    "id": 2
                }
            ]
        },
        {
            "name": "KnapsackListRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "data",
                    "name": "consume_list",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "data",
                    "name": "material_list",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "data",
                    "name": "hero_fragment_list",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "data",
                    "name": "equip_fragment_list",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "data",
                    "name": "horse_list",
                    "id": 5
                },
                {
                    "rule": "repeated",
                    "type": "data",
                    "name": "talisman_list",
                    "id": 6
                }
            ],
            "messages": [
                {
                    "name": "data",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "id",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "num",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "make_time",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "int32",
                            "name": "k_type",
                            "id": 4
                        }
                    ]
                }
            ]
        },
        {
            "name": "KnapsackOperationReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "kid",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "num",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "choose",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "KOflag",
                    "name": "flag",
                    "id": 4
                }
            ]
        },
        {
            "name": "KnapsackOperationRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "KOflag",
                    "name": "flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "rewards",
                    "id": 2
                }
            ]
        },
        {
            "name": "HeroUpgradeReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "hero_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint64",
                    "name": "level",
                    "id": 2
                }
            ]
        },
        {
            "name": "HeroStarUpReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "hero_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "act_type",
                    "id": 2
                }
            ]
        },
        {
            "name": "HeroAwakeUpReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "hero_id",
                    "id": 1
                }
            ]
        },
        {
            "name": "EquipmentEnhancedReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "equipment_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "enhance_times",
                    "id": 2
                }
            ]
        },
        {
            "name": "EquipmentEnhancedRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "Equipment",
                    "name": "equipment",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "status",
                    "id": 2
                }
            ]
        },
        {
            "name": "EquipmentSwitchReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "heroId",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "dst",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "src",
                    "id": 3
                }
            ]
        },
        {
            "name": "EquipmentSyntheticReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "eType",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "eids",
                    "id": 2,
                    "options": {
                        "packed": true
                    }
                }
            ]
        },
        {
            "name": "EquipmentSyntheticByMaterialReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "eType",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "count",
                    "id": 2
                }
            ]
        },
        {
            "name": "EquipmentStarUpReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "eid",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "eids",
                    "id": 2,
                    "options": {
                        "packed": true
                    }
                }
            ]
        },
        {
            "name": "EquipmentStarUpRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "Equipment",
                    "name": "equipment",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "status",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "recyle",
                    "id": 3
                }
            ]
        },
        {
            "name": "GetSquadRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "SquadInfo",
                    "keytype": "int32",
                    "name": "user_squad",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "main",
                    "id": 2
                }
            ],
            "messages": [
                {
                    "name": "SquadInfo",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "name",
                            "id": 1
                        },
                        {
                            "rule": "repeated",
                            "type": "int64",
                            "name": "squad",
                            "id": 2,
                            "options": {
                                "packed": true
                            }
                        }
                    ]
                }
            ]
        },
        {
            "name": "Equipment",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "e_type",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint64",
                    "name": "enhanced_lv",
                    "id": 2
                },
                {
                    "rule": "map",
                    "type": "int64",
                    "keytype": "int32",
                    "name": "enhanced_then_prop",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "belong",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "star_lv",
                    "id": 5
                }
            ]
        },
        {
            "name": "PubReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "pub_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "PubFlag",
                    "name": "num",
                    "id": 2
                }
            ]
        },
        {
            "name": "PubRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "pub",
                    "name": "list",
                    "id": 1
                },
                {
                    "rule": "map",
                    "type": "PubUsedInfo",
                    "keytype": "int64",
                    "name": "used",
                    "id": 2
                }
            ],
            "messages": [
                {
                    "name": "pub",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "goods_id",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "goods_num",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "bool",
                            "name": "is_hero",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "bool",
                            "name": "is_fragment",
                            "id": 4
                        }
                    ]
                }
            ]
        },
        {
            "name": "HeroRecruitReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "hero_id",
                    "id": 1
                }
            ]
        },
        {
            "name": "HeroRecruitRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "hero_id",
                    "id": 1
                }
            ]
        },
        {
            "name": "GetPubTimeRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "PubUsedInfo",
                    "keytype": "int64",
                    "name": "used",
                    "id": 2
                }
            ]
        },
        {
            "name": "PubUsedInfo",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "surplus_time",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "surplus_num",
                    "id": 2
                }
            ]
        },
        {
            "name": "GetSignInfoReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "act_id",
                    "id": 1
                }
            ]
        },
        {
            "name": "GetSignInfoRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "data",
                    "name": "list",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "now_day",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "surplus_day",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "days",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "max_days",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "gift_id",
                    "id": 6
                }
            ],
            "messages": [
                {
                    "name": "data",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "finish",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "good_id",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "good_num",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "uint64",
                            "name": "vip_level",
                            "id": 4
                        }
                    ]
                }
            ]
        },
        {
            "name": "GetSignSevenRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "ActList",
                    "keytype": "int64",
                    "name": "act_list",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "ActList",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "reward",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "start_time",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "uint32",
                            "name": "surplus_day",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "now_reward",
                            "id": 4
                        },
                        {
                            "rule": "map",
                            "type": "int64",
                            "keytype": "int64",
                            "name": "record",
                            "id": 5
                        }
                    ]
                }
            ]
        },
        {
            "name": "SetSignInReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "act_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "index",
                    "id": 2
                }
            ]
        },
        {
            "name": "SetSevenSignInRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "pri_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "is_sign",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "finish",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "rewards",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "special_rewards",
                    "id": 5
                }
            ]
        },
        {
            "name": "SetSignInRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "index",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "finish",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "rewards",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "is_sign",
                    "id": 4
                }
            ]
        },
        {
            "name": "ReceivingGiftReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "act_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "index",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "RevGiftType",
                    "name": "r_type",
                    "id": 3
                }
            ]
        },
        {
            "name": "ReceivingGiftRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "gift_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "days",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "max_days",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "rewards",
                    "id": 6
                }
            ]
        },
        {
            "name": "GetTaskListRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Task",
                    "name": "main_task",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Task",
                    "name": "daily_task",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "liveness",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "live_reward",
                    "id": 4
                }
            ],
            "messages": [
                {
                    "name": "Task",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "task_id",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "process",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "TaskStatus",
                            "name": "status",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "RankReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "RankType",
                    "name": "type",
                    "id": 1
                }
            ]
        },
        {
            "name": "RankListRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "data",
                    "name": "list",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "data",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "id",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "rank",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "nickname",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "avatar",
                            "id": 4
                        },
                        {
                            "rule": "optional",
                            "type": "uint64",
                            "name": "level",
                            "id": 5
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "exp",
                            "id": 6
                        },
                        {
                            "rule": "optional",
                            "type": "uint64",
                            "name": "fight",
                            "id": 7
                        }
                    ]
                }
            ]
        },
        {
            "name": "GetPayInfoRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "bool",
                    "keytype": "int64",
                    "name": "list",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "record",
                    "id": 2
                }
            ]
        },
        {
            "name": "UserVipGiftRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "list",
                    "id": 1,
                    "options": {
                        "packed": true
                    }
                }
            ]
        },
        {
            "name": "GetFastBuyInfoRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "uint32",
                    "keytype": "uint32",
                    "name": "list",
                    "id": 1
                }
            ]
        },
        {
            "name": "FastBuyReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "FastBuyType",
                    "name": "fast_buy_type",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "FastBuyCoinType",
                    "name": "coin_type",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "num",
                    "id": 3
                }
            ]
        },
        {
            "name": "FastBuyRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "FastBuyType",
                    "name": "fast_buy_type",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "surplus_num",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "critical",
                    "id": 3,
                    "options": {
                        "packed": true
                    }
                }
            ]
        },
        {
            "name": "UserVipReceiveGiftReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "id",
                    "id": 1
                }
            ]
        },
        {
            "name": "UserVipReceiveGiftRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "finish",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "rewards",
                    "id": 3
                }
            ]
        },
        {
            "name": "AutoIdReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "id",
                    "id": 1
                }
            ]
        },
        {
            "name": "GetActListRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "ActList",
                    "keytype": "int64",
                    "name": "act_list",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "Opers",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "op_id",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "process",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "TaskStatus",
                            "name": "status",
                            "id": 3
                        },
                        {
                            "rule": "map",
                            "type": "int64",
                            "keytype": "int64",
                            "name": "record",
                            "id": 4
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "reward",
                            "id": 5
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "surplus_time",
                            "id": 6
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "extends",
                            "id": 7
                        }
                    ]
                },
                {
                    "name": "ActList",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "repeated",
                            "type": "Opers",
                            "name": "op_list",
                            "id": 1
                        }
                    ]
                }
            ]
        },
        {
            "name": "AddOrderReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "flag",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "id",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "op_id",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "re_id",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "pri_id",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "env",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "platform",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "source",
                    "id": 8
                }
            ]
        },
        {
            "name": "AddOrderRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "order_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "extend",
                    "id": 2
                }
            ]
        },
        {
            "name": "ReChangeReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "order_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "extend",
                    "id": 2
                }
            ]
        },
        {
            "name": "ReChangeRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "act_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "op_id",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "pri_id",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "TaskStatus",
                    "name": "status",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "reward",
                    "name": "rewards",
                    "id": 5
                }
            ],
            "messages": [
                {
                    "name": "reward",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "id",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "num",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "bool",
                            "name": "is_hero",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "SpiritSupplayInfo",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "SpiritSupplay",
                    "name": "status",
                    "id": 1,
                    "options": {
                        "packed": true
                    }
                },
                {
                    "rule": "optional",
                    "type": "Item",
                    "name": "item",
                    "id": 2
                }
            ]
        },
        {
            "name": "RecycleReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "RecycleFlag",
                    "name": "flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "ids",
                    "id": 2,
                    "options": {
                        "packed": true
                    }
                }
            ]
        },
        {
            "name": "Adventure",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "process",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "duration",
                    "id": 3
                }
            ]
        },
        {
            "name": "SuppressBanditStates",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "Bandit",
                    "keytype": "int64",
                    "name": "bandit",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "process",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "history",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "uint32",
                    "name": "box",
                    "id": 4,
                    "options": {
                        "packed": true
                    }
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "adventure_count",
                    "id": 5
                }
            ],
            "messages": [
                {
                    "name": "Bandit",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "hp",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "process",
                            "id": 2
                        }
                    ]
                }
            ]
        },
        {
            "name": "AdventureList",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "Adventure",
                    "keytype": "int64",
                    "name": "adventure",
                    "id": 1
                }
            ]
        },
        {
            "name": "SuppressBanditAttackRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "Item",
                    "name": "coin",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "critical",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "damage",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "pass_reward",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "hp",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "process",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "new_adventure",
                    "id": 7
                }
            ]
        },
        {
            "name": "SuppressBanditSweepRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "SweepRewards",
                    "name": "sweep_rewards",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "new_adventure",
                    "id": 2,
                    "options": {
                        "packed": true
                    }
                },
                {
                    "rule": "optional",
                    "type": "SuppressBanditStates",
                    "name": "state",
                    "id": 3
                }
            ],
            "messages": [
                {
                    "name": "SweepRewards",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "index",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "sbtoken_cost",
                            "id": 2
                        },
                        {
                            "rule": "repeated",
                            "type": "Item",
                            "name": "reward",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "AdventureUpdateReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "adventure_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "op",
                    "id": 2
                }
            ]
        },
        {
            "name": "AdventureUpdateRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "Adventure",
                    "keytype": "int64",
                    "name": "adventure_list",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "reward",
                    "id": 2
                }
            ]
        },
        {
            "name": "AddPrincipalReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "principal_gsid",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "principal_uid",
                    "id": 2
                }
            ]
        },
        {
            "name": "GetRedPacketListRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "number",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "balance",
                    "id": 2
                },
                {
                    "rule": "map",
                    "type": "red_packet",
                    "keytype": "string",
                    "name": "list",
                    "id": 3
                }
            ],
            "messages": [
                {
                    "name": "red_packet",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "money",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "name",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "level",
                            "id": 4
                        },
                        {
                            "rule": "optional",
                            "type": "bool",
                            "name": "mine",
                            "id": 5
                        }
                    ]
                }
            ]
        },
        {
            "name": "OpenRedPacketReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "rpid",
                    "id": 1
                }
            ]
        },
        {
            "name": "OpenRedPacketRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "balance",
                    "id": 1
                }
            ]
        },
        {
            "name": "OperateRedPacketReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "money",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "RedPacketOperateFlag",
                    "name": "operate",
                    "id": 2
                }
            ]
        },
        {
            "name": "OperateRedPacketRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "balance",
                    "id": 1
                }
            ]
        },
        {
            "name": "GetPatronsListRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Patron",
                    "name": "Patrons",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "Patron",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "name",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "uint64",
                            "name": "level",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "avatar",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "ActRevRewardReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "act_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "op_id",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "pri_id",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "reward",
                    "id": 4
                }
            ]
        },
        {
            "name": "BossGetBossDataRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "BossUsed",
                    "keytype": "int64",
                    "name": "boss_list",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "BossUsed",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "UsedBlood",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "SumBlood",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "PersonNum",
                            "id": 3
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "RebirthTime",
                            "id": 4
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "user_rank",
                            "id": 5
                        }
                    ]
                }
            ]
        },
        {
            "name": "BossFightStartReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "bs_id",
                    "id": 1
                }
            ]
        },
        {
            "name": "BossFightStartRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Fight",
                    "name": "my_info",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Fight",
                    "name": "boss_info",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "UsedBlood",
                    "id": 3
                }
            ]
        },
        {
            "name": "BossFightEndReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "bs_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "bs_blood",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "is_kill",
                    "id": 3
                }
            ]
        },
        {
            "name": "BossFightEndRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "fight_rewards",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "kill_rewards",
                    "id": 2
                }
            ]
        },
        {
            "name": "BossFightRankReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "bs_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "page_index",
                    "id": 2
                }
            ]
        },
        {
            "name": "BossFightRankRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Rank",
                    "name": "rank_list",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "last_page",
                    "id": 2
                }
            ],
            "messages": [
                {
                    "name": "Rank",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int32",
                            "name": "rank",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "string",
                            "name": "nickname",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "double",
                            "name": "score",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "GetUserSpiritRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "spirit",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "left",
                    "id": 2
                }
            ]
        },
        {
            "name": "OperateRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "status",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "OperateType",
                    "name": "type",
                    "id": 2
                }
            ]
        },
        {
            "name": "ErrorRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "msg_id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "status",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "description",
                    "id": 3
                }
            ]
        },
        {
            "name": "ComingFightReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "id",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "FightModel",
                    "name": "model",
                    "id": 2
                }
            ]
        },
        {
            "name": "UserNotice",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "uint64",
                    "name": "level",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "exp",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "uint64",
                    "name": "Fp",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "uint32",
                    "name": "vipLevel",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "vip_exp",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "spirit",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "coin",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "ingots",
                    "id": 8
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "goods_coin",
                    "id": 9
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "weapon_coin",
                    "id": 10
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "BossFightTimes",
                    "id": 11
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "sbToken",
                    "id": 12
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "star_coin",
                    "id": 13
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "eq_star_stone",
                    "id": 14
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "refresh_coin",
                    "id": 15
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "horse_coin",
                    "id": 16
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "talisman_coin",
                    "id": 17
                }
            ]
        },
        {
            "name": "UpKnapsackNotice",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "up_info",
                    "keytype": "int64",
                    "name": "up_list",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "up_info",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "surplus_num",
                            "id": 1
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "make_time",
                            "id": 2
                        },
                        {
                            "rule": "optional",
                            "type": "int64",
                            "name": "k_type",
                            "id": 3
                        }
                    ]
                }
            ]
        },
        {
            "name": "UpStoreNotice",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "int64",
                    "keytype": "int64",
                    "name": "up_list",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "StoreType",
                    "name": "store_type",
                    "id": 2
                }
            ]
        },
        {
            "name": "UpdateEquipmentRepoNotice",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "map",
                    "type": "Equipment",
                    "keytype": "int64",
                    "name": "add_or_update",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "remove",
                    "id": 2,
                    "options": {
                        "packed": true
                    }
                }
            ]
        },
        {
            "name": "SendQualifyingRankNotice",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "result",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "new_rank",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "challenger_name",
                    "id": 3
                }
            ]
        },
        {
            "name": "SendMailUnreadNotice",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "has_unread",
                    "id": 1
                }
            ]
        },
        {
            "name": "SendHerosNotice",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "UserHero",
                    "name": "update_heros",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "remove",
                    "id": 2,
                    "options": {
                        "packed": true
                    }
                }
            ]
        },
        {
            "name": "RedDotNotice",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "List",
                    "name": "list",
                    "id": 1
                }
            ],
            "messages": [
                {
                    "name": "List",
                    "syntax": "proto3",
                    "fields": [
                        {
                            "rule": "repeated",
                            "type": "int64",
                            "name": "val",
                            "id": 1,
                            "options": {
                                "packed": true
                            }
                        }
                    ]
                }
            ]
        },
        {
            "name": "RedPacketNotice",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "has",
                    "id": 1
                }
            ]
        },
        {
            "name": "MsgNotice",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "UpKnapsackNotice",
                    "name": "kna",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "UpStoreNotice",
                    "name": "store",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "UpdateEquipmentRepoNotice",
                    "name": "equipment",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "SendQualifyingRankNotice",
                    "name": "qualifying",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "SendHerosNotice",
                    "name": "hero",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "RedDotNotice",
                    "name": "red_dot",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "UserNotice",
                    "name": "user",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "RedPacketNotice",
                    "name": "rp",
                    "id": 8
                }
            ]
        },
        {
            "name": "StarsStatusRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "status",
                    "id": 1
                }
            ]
        },
        {
            "name": "StarsOperateReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "StarsOperateFlag",
                    "name": "stars_operate_req",
                    "id": 1
                }
            ]
        },
        {
            "name": "StarsOperateRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "StarsOperateFlag",
                    "name": "stars_operate_res",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "next_status",
                    "id": 2
                }
            ]
        },
        {
            "name": "LotteryCountRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "free_count",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "video_count",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "items",
                    "id": 3
                }
            ]
        },
        {
            "name": "LotteryRunRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "free_count",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "video_count",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "items",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "index",
                    "id": 4
                }
            ]
        },
        {
            "name": "StatusReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "type",
                    "id": 1
                }
            ]
        },
        {
            "name": "StatusRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "type",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "status",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "status_awards",
                    "id": 3
                }
            ]
        },
        {
            "name": "AwardsReq",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "type",
                    "id": 1
                }
            ]
        },
        {
            "name": "AwardsRes",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "type",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "Item",
                    "name": "items",
                    "id": 2
                }
            ]
        }
    ],
    "enums": [
        {
            "name": "ApiCode",
            "syntax": "proto3",
            "values": [
                {
                    "name": "default_cod",
                    "id": 0
                },
                {
                    "name": "sync_user_info_req",
                    "id": 1001
                },
                {
                    "name": "sync_user_info_res",
                    "id": 1002
                },
                {
                    "name": "player_info_init_req",
                    "id": 1003
                },
                {
                    "name": "user_hero_list_res",
                    "id": 1006
                },
                {
                    "name": "announcement_req",
                    "id": 1009
                },
                {
                    "name": "announcement_res",
                    "id": 1010
                },
                {
                    "name": "update_user_info_req",
                    "id": 1011
                },
                {
                    "name": "update_user_info_res",
                    "id": 1012
                },
                {
                    "name": "get_mail_list_req",
                    "id": 1013
                },
                {
                    "name": "get_mail_list_res",
                    "id": 1014
                },
                {
                    "name": "mail_marked_req",
                    "id": 1015
                },
                {
                    "name": "mail_reward_req",
                    "id": 1017
                },
                {
                    "name": "mail_reward_res",
                    "id": 1018
                },
                {
                    "name": "store_list_req",
                    "id": 1019
                },
                {
                    "name": "store_list_res",
                    "id": 1020
                },
                {
                    "name": "buy_goods_req",
                    "id": 1021
                },
                {
                    "name": "knapsack_list_res",
                    "id": 1024
                },
                {
                    "name": "knapsack_operation_req",
                    "id": 1025
                },
                {
                    "name": "knapsack_operation_res",
                    "id": 1026
                },
                {
                    "name": "dungeon_fight_begin_req",
                    "id": 1027
                },
                {
                    "name": "dungeon_fight_begin_res",
                    "id": 1028
                },
                {
                    "name": "dungeon_state_req",
                    "id": 1031
                },
                {
                    "name": "dungeon_state_res",
                    "id": 1032
                },
                {
                    "name": "hero_upgrade_req",
                    "id": 1033
                },
                {
                    "name": "hero_star_up_req",
                    "id": 1035
                },
                {
                    "name": "hero_awake_up_req",
                    "id": 1037
                },
                {
                    "name": "dungeon_fight_end_req",
                    "id": 1039
                },
                {
                    "name": "dungeon_fight_end_res",
                    "id": 1040
                },
                {
                    "name": "receiving_box_req",
                    "id": 1041
                },
                {
                    "name": "receiving_box_res",
                    "id": 1042
                },
                {
                    "name": "dungeon_sweep_req",
                    "id": 1043
                },
                {
                    "name": "dungeon_sweep_res",
                    "id": 1044
                },
                {
                    "name": "change_user_squad_req",
                    "id": 1045
                },
                {
                    "name": "equipment_enhanced_req",
                    "id": 1047
                },
                {
                    "name": "equipment_enhanced_res",
                    "id": 1048
                },
                {
                    "name": "equipment_switch_req",
                    "id": 1049
                },
                {
                    "name": "equipment_synthetic_req",
                    "id": 1051
                },
                {
                    "name": "equipment_synthetic_res",
                    "id": 1052
                },
                {
                    "name": "get_squad_req",
                    "id": 1053
                },
                {
                    "name": "get_squad_res",
                    "id": 1054
                },
                {
                    "name": "pub_req",
                    "id": 1055
                },
                {
                    "name": "pub_res",
                    "id": 1056
                },
                {
                    "name": "hero_recruit_req",
                    "id": 1057
                },
                {
                    "name": "hero_recruit_res",
                    "id": 1058
                },
                {
                    "name": "get_pub_time_req",
                    "id": 1059
                },
                {
                    "name": "get_pub_time_res",
                    "id": 1060
                },
                {
                    "name": "qualifying_refresh_req",
                    "id": 1061
                },
                {
                    "name": "qualifying_refresh_res",
                    "id": 1062
                },
                {
                    "name": "qualifying_fight_begin_req",
                    "id": 1063
                },
                {
                    "name": "qualifying_fight_begin_res",
                    "id": 1064
                },
                {
                    "name": "qualifying_fight_end_req",
                    "id": 1065
                },
                {
                    "name": "qualifying_fight_end_res",
                    "id": 1066
                },
                {
                    "name": "get_qualifying_state_req",
                    "id": 1067
                },
                {
                    "name": "get_qualifying_state_res",
                    "id": 1068
                },
                {
                    "name": "get_qualifying_record_req",
                    "id": 1069
                },
                {
                    "name": "get_qualifying_record_res",
                    "id": 1070
                },
                {
                    "name": "equipment_synthetic_byMaterial_req",
                    "id": 1071
                },
                {
                    "name": "equipment_synthetic_byMaterial_res",
                    "id": 1072
                },
                {
                    "name": "rank_req",
                    "id": 1073
                },
                {
                    "name": "rank_list_res",
                    "id": 1074
                },
                {
                    "name": "get_pay_info_req",
                    "id": 1075
                },
                {
                    "name": "get_pay_info_res",
                    "id": 1076
                },
                {
                    "name": "user_vip_gift_req",
                    "id": 1079
                },
                {
                    "name": "user_vip_gift_res",
                    "id": 1080
                },
                {
                    "name": "user_vip_receive_gift_req",
                    "id": 1081
                },
                {
                    "name": "user_vip_receive_gift_res",
                    "id": 1082
                },
                {
                    "name": "get_fast_buy_info_req",
                    "id": 1083
                },
                {
                    "name": "get_fast_buy_info_res",
                    "id": 1084
                },
                {
                    "name": "fast_buy_req",
                    "id": 1085
                },
                {
                    "name": "fast_buy_res",
                    "id": 1086
                },
                {
                    "name": "task_list_req",
                    "id": 1087
                },
                {
                    "name": "task_list_res",
                    "id": 1088
                },
                {
                    "name": "task_finish_req",
                    "id": 1089
                },
                {
                    "name": "task_finish_res",
                    "id": 1090
                },
                {
                    "name": "task_active_reward_req",
                    "id": 1091
                },
                {
                    "name": "task_active_reward_res",
                    "id": 1092
                },
                {
                    "name": "game_sharing_req",
                    "id": 1093
                },
                {
                    "name": "game_sharing_res",
                    "id": 1094
                },
                {
                    "name": "equipment_quick_enhance_req",
                    "id": 1095
                },
                {
                    "name": "equipment_quick_enhance_res",
                    "id": 1096
                },
                {
                    "name": "equipment_quick_switch_req",
                    "id": 1097
                },
                {
                    "name": "equipment_quick_switch_res",
                    "id": 1098
                },
                {
                    "name": "new_user_guide_req",
                    "id": 1099
                },
                {
                    "name": "new_user_guide_res",
                    "id": 1100
                },
                {
                    "name": "get_tower_state_req",
                    "id": 1101
                },
                {
                    "name": "get_tower_state_res",
                    "id": 1102
                },
                {
                    "name": "tower_fight_begin_req",
                    "id": 1103
                },
                {
                    "name": "tower_fight_begin_res",
                    "id": 1104
                },
                {
                    "name": "tower_fight_end_req",
                    "id": 1105
                },
                {
                    "name": "tower_fight_end_res",
                    "id": 1106
                },
                {
                    "name": "tower_open_box_req",
                    "id": 1107
                },
                {
                    "name": "tower_open_box_res",
                    "id": 1108
                },
                {
                    "name": "tower_sweep_req",
                    "id": 1109
                },
                {
                    "name": "tower_sweep_res",
                    "id": 1110
                },
                {
                    "name": "tower_reset_req",
                    "id": 1111
                },
                {
                    "name": "tower_reset_res",
                    "id": 1112
                },
                {
                    "name": "tower_sweep_surplus_time_req",
                    "id": 1113
                },
                {
                    "name": "tower_sweep_surplus_time_res",
                    "id": 1114
                },
                {
                    "name": "tower_clear_box_req",
                    "id": 1115
                },
                {
                    "name": "tower_clear_box_res",
                    "id": 1116
                },
                {
                    "name": "qualifying_competitor_state_req",
                    "id": 1119
                },
                {
                    "name": "qualifying_competitor_state_res",
                    "id": 1120
                },
                {
                    "name": "recycle_req",
                    "id": 1121
                },
                {
                    "name": "recycle_res",
                    "id": 1122
                },
                {
                    "name": "spirit_supply_state_req",
                    "id": 1123
                },
                {
                    "name": "spirit_supply_state_res",
                    "id": 1124
                },
                {
                    "name": "rev_spirit_supply_req",
                    "id": 1125
                },
                {
                    "name": "rev_spirit_supply_res",
                    "id": 1126
                },
                {
                    "name": "squad_show_req",
                    "id": 1127
                },
                {
                    "name": "squad_show_res",
                    "id": 1128
                },
                {
                    "name": "boss_get_boss_data_req",
                    "id": 1129
                },
                {
                    "name": "boss_get_boss_data_res",
                    "id": 1130
                },
                {
                    "name": "boss_fight_start_req",
                    "id": 1131
                },
                {
                    "name": "boss_fight_start_res",
                    "id": 1132
                },
                {
                    "name": "boss_fight_end_req",
                    "id": 1133
                },
                {
                    "name": "boss_fight_end_res",
                    "id": 1134
                },
                {
                    "name": "boss_fight_rank_req",
                    "id": 1135
                },
                {
                    "name": "boss_fight_rank_res",
                    "id": 1136
                },
                {
                    "name": "suppress_bandit_state_req",
                    "id": 1137
                },
                {
                    "name": "suppress_bandit_state_res",
                    "id": 1138
                },
                {
                    "name": "suppress_bandit_attack_req",
                    "id": 1139
                },
                {
                    "name": "suppress_bandit_attack_res",
                    "id": 1140
                },
                {
                    "name": "suppress_bandit_fight_begin_req",
                    "id": 1141
                },
                {
                    "name": "suppress_bandit_fight_begin_res",
                    "id": 1142
                },
                {
                    "name": "suppress_bandit_fight_end_req",
                    "id": 1143
                },
                {
                    "name": "suppress_bandit_fight_end_res",
                    "id": 1144
                },
                {
                    "name": "suppress_bandit_sweep_req",
                    "id": 1145
                },
                {
                    "name": "suppress_bandit_sweep_res",
                    "id": 1146
                },
                {
                    "name": "suppress_bandit_open_req",
                    "id": 1147
                },
                {
                    "name": "suppress_bandit_open_res",
                    "id": 1148
                },
                {
                    "name": "coming_to_fight_req",
                    "id": 1149
                },
                {
                    "name": "coming_to_fight_res",
                    "id": 1150
                },
                {
                    "name": "adventure_update_req",
                    "id": 1151
                },
                {
                    "name": "adventure_update_res",
                    "id": 1152
                },
                {
                    "name": "stars_status_req",
                    "id": 1153
                },
                {
                    "name": "stars_status_res",
                    "id": 1154
                },
                {
                    "name": "stars_operation_req",
                    "id": 1155
                },
                {
                    "name": "stars_operation_res",
                    "id": 1156
                },
                {
                    "name": "adventure_list_req",
                    "id": 1157
                },
                {
                    "name": "adventure_list_res",
                    "id": 1158
                },
                {
                    "name": "equipment_star_up_req",
                    "id": 1159
                },
                {
                    "name": "equipment_star_up_res",
                    "id": 1160
                },
                {
                    "name": "get_user_spirit_req",
                    "id": 1161
                },
                {
                    "name": "get_user_spirit_res",
                    "id": 1162
                },
                {
                    "name": "store_new_list_req",
                    "id": 1163
                },
                {
                    "name": "store_new_list_res",
                    "id": 1164
                },
                {
                    "name": "buy_new_goods_req",
                    "id": 1165
                },
                {
                    "name": "lottery_count_req",
                    "id": 1167
                },
                {
                    "name": "lottery_count_res",
                    "id": 1168
                },
                {
                    "name": "lottery_add_count_req",
                    "id": 1169
                },
                {
                    "name": "lottery_add_count_res",
                    "id": 1170
                },
                {
                    "name": "lottery_run_req",
                    "id": 1171
                },
                {
                    "name": "lottery_run_res",
                    "id": 1172
                },
                {
                    "name": "status_req",
                    "id": 1173
                },
                {
                    "name": "status_res",
                    "id": 1174
                },
                {
                    "name": "awards_req",
                    "id": 1175
                },
                {
                    "name": "awards_res",
                    "id": 1176
                },
                {
                    "name": "game_error_notice",
                    "id": 2000
                },
                {
                    "name": "currency_notice",
                    "id": 3000
                },
                {
                    "name": "operate_res",
                    "id": 3001
                },
                {
                    "name": "up_knapsack_notice",
                    "id": 3002
                },
                {
                    "name": "update_equipment_repo_notice",
                    "id": 3003
                },
                {
                    "name": "send_qualifying_rank_notice",
                    "id": 3004
                },
                {
                    "name": "send_user_notice",
                    "id": 3005
                },
                {
                    "name": "send_store_notice",
                    "id": 3006
                },
                {
                    "name": "send_hero_grow_up_notice",
                    "id": 3007
                },
                {
                    "name": "send_activity_notice",
                    "id": 3008
                },
                {
                    "name": "get_sign_info_req",
                    "id": 4001
                },
                {
                    "name": "get_sign_info_res",
                    "id": 4002
                },
                {
                    "name": "set_sign_in_req",
                    "id": 4003
                },
                {
                    "name": "set_sign_in_res",
                    "id": 4004
                },
                {
                    "name": "receiving_gift_req",
                    "id": 4005
                },
                {
                    "name": "receiving_gift_res",
                    "id": 4006
                },
                {
                    "name": "get_act_list_req",
                    "id": 4007
                },
                {
                    "name": "get_act_list_res",
                    "id": 4008
                },
                {
                    "name": "order_req",
                    "id": 4009
                },
                {
                    "name": "order_res",
                    "id": 4010
                },
                {
                    "name": "recharge_req",
                    "id": 4011
                },
                {
                    "name": "recharge_res",
                    "id": 4012
                },
                {
                    "name": "act_rev_reward_req",
                    "id": 4013
                },
                {
                    "name": "act_rev_reward_res",
                    "id": 4014
                },
                {
                    "name": "add_principal_req",
                    "id": 4015
                },
                {
                    "name": "add_principal_res",
                    "id": 4016
                },
                {
                    "name": "get_red_packet_list_req",
                    "id": 4017
                },
                {
                    "name": "get_red_packet_list_res",
                    "id": 4018
                },
                {
                    "name": "open_red_packet_req",
                    "id": 4019
                },
                {
                    "name": "open_red_packet_res",
                    "id": 4020
                },
                {
                    "name": "operate_red_packet_req",
                    "id": 4021
                },
                {
                    "name": "operate_red_packet_res",
                    "id": 4022
                },
                {
                    "name": "get_patrons_req",
                    "id": 4023
                },
                {
                    "name": "get_patrons_res",
                    "id": 4024
                }
            ]
        },
        {
            "name": "FightModel",
            "syntax": "proto3",
            "values": [
                {
                    "name": "Dungeon",
                    "id": 0
                },
                {
                    "name": "Tower",
                    "id": 1
                },
                {
                    "name": "Qualifying",
                    "id": 2
                },
                {
                    "name": "AdventureKill",
                    "id": 3
                },
                {
                    "name": "Boss",
                    "id": 4
                },
                {
                    "name": "SuppressBanditBoss",
                    "id": 5
                }
            ]
        },
        {
            "name": "KOflag",
            "syntax": "proto3",
            "values": [
                {
                    "name": "DefType",
                    "id": 0
                },
                {
                    "name": "SellType",
                    "id": 1
                },
                {
                    "name": "UsedType",
                    "id": 2
                }
            ]
        },
        {
            "name": "RecycleFlag",
            "syntax": "proto3",
            "values": [
                {
                    "name": "DefRecycleType",
                    "id": 0
                },
                {
                    "name": "HeroRecycleType",
                    "id": 1
                },
                {
                    "name": "HeroFragmentRecycleType",
                    "id": 2
                },
                {
                    "name": "EquipmentRecycleType",
                    "id": 3
                },
                {
                    "name": "EquipmentRecycleFragmentType",
                    "id": 4
                }
            ]
        },
        {
            "name": "Flag",
            "syntax": "proto3",
            "values": [
                {
                    "name": "MailDefault",
                    "id": 0
                },
                {
                    "name": "MailUnRead",
                    "id": 1
                },
                {
                    "name": "MailReaded",
                    "id": 2
                },
                {
                    "name": "MailDelete",
                    "id": 3
                },
                {
                    "name": "MailReward",
                    "id": 4
                }
            ]
        },
        {
            "name": "DungeonDifficult",
            "syntax": "proto3",
            "values": [
                {
                    "name": "Dungeon_Unavailable",
                    "id": 0
                },
                {
                    "name": "Dungeon_Simple",
                    "id": 1
                },
                {
                    "name": "Dungeon_Hard",
                    "id": 2
                },
                {
                    "name": "Dungeon_Hero",
                    "id": 3
                }
            ]
        },
        {
            "name": "DungeonPassStar",
            "syntax": "proto3",
            "values": [
                {
                    "name": "Star0",
                    "id": 0
                },
                {
                    "name": "Star1",
                    "id": 1
                },
                {
                    "name": "Star2",
                    "id": 2
                },
                {
                    "name": "Star3",
                    "id": 3
                }
            ]
        },
        {
            "name": "TaskStatus",
            "syntax": "proto3",
            "values": [
                {
                    "name": "DefaultStatus",
                    "id": 0
                },
                {
                    "name": "Underway",
                    "id": 1
                },
                {
                    "name": "Finish",
                    "id": 2
                },
                {
                    "name": "Over",
                    "id": 3
                }
            ]
        },
        {
            "name": "StoreType",
            "syntax": "proto3",
            "values": [
                {
                    "name": "StoreDefaultType",
                    "id": 0
                },
                {
                    "name": "StoreIngotsType",
                    "id": 1
                },
                {
                    "name": "StoreRankType",
                    "id": 2
                },
                {
                    "name": "StoreEquipmentType",
                    "id": 3
                },
                {
                    "name": "StoreWarHorseType",
                    "id": 4
                },
                {
                    "name": "StoreTalismanType",
                    "id": 5
                }
            ]
        },
        {
            "name": "RefreshType",
            "syntax": "proto3",
            "values": [
                {
                    "name": "RefreshDefaultType",
                    "id": 0
                },
                {
                    "name": "StoreHandRefreshType",
                    "id": 1
                }
            ]
        },
        {
            "name": "RevGiftType",
            "syntax": "proto3",
            "values": [
                {
                    "name": "SignType",
                    "id": 0
                },
                {
                    "name": "VipGiftType",
                    "id": 1
                },
                {
                    "name": "DaysGiftType",
                    "id": 2
                }
            ]
        },
        {
            "name": "RankType",
            "syntax": "proto3",
            "values": [
                {
                    "name": "DefaultRankType",
                    "id": 0
                },
                {
                    "name": "LevelType",
                    "id": 1
                },
                {
                    "name": "FightType",
                    "id": 2
                },
                {
                    "name": "PaiWeiType",
                    "id": 3
                }
            ]
        },
        {
            "name": "FastBuyType",
            "syntax": "proto3",
            "values": [
                {
                    "name": "DefaultBuy",
                    "id": 0
                },
                {
                    "name": "BuySpirit",
                    "id": 1
                },
                {
                    "name": "BuyCoin",
                    "id": 2
                },
                {
                    "name": "BuyTimes",
                    "id": 3
                },
                {
                    "name": "BuyPub",
                    "id": 4
                },
                {
                    "name": "BuyBossTimes",
                    "id": 5
                },
                {
                    "name": "BuyJiaoFeiLiTimes",
                    "id": 6
                }
            ]
        },
        {
            "name": "FastBuyCoinType",
            "syntax": "proto3",
            "values": [
                {
                    "name": "OnceBuy",
                    "id": 0
                },
                {
                    "name": "TenBuy",
                    "id": 1
                }
            ]
        },
        {
            "name": "OperateType",
            "syntax": "proto3",
            "values": [
                {
                    "name": "Default",
                    "id": 0
                },
                {
                    "name": "Upgrade",
                    "id": 1
                }
            ]
        },
        {
            "name": "PubFlag",
            "syntax": "proto3",
            "values": [
                {
                    "name": "PubDefault",
                    "id": 0
                },
                {
                    "name": "PubOne",
                    "id": 1
                },
                {
                    "name": "PubTen",
                    "id": 10
                }
            ]
        },
        {
            "name": "RedPacketOperateFlag",
            "syntax": "proto3",
            "values": [
                {
                    "name": "RPDefault",
                    "id": 0
                },
                {
                    "name": "Recharge",
                    "id": 1
                },
                {
                    "name": "Withdraw",
                    "id": 2
                }
            ]
        },
        {
            "name": "SpiritSupplay",
            "syntax": "proto3",
            "values": [
                {
                    "name": "RevDefault",
                    "id": 0
                },
                {
                    "name": "InRev",
                    "id": 1
                },
                {
                    "name": "Revd",
                    "id": 2
                },
                {
                    "name": "OutRev",
                    "id": 3
                }
            ]
        },
        {
            "name": "StarsOperateFlag",
            "syntax": "proto3",
            "values": [
                {
                    "name": "StarsOperateDefault",
                    "id": 0
                },
                {
                    "name": "StarUp",
                    "id": 1
                },
                {
                    "name": "StarUpAll",
                    "id": 2
                }
            ]
        }
    ],
    "isNamespace": true
}).build();