/**
 * Author: ling_feng
 * Date: 2018.5.16
 * CopyRight:
 * 场景管理
 */

import MKLog from './MKLog';


cc.game.on(cc.game.EVENT_HIDE, function () {
    MKLog.log('切到后台了');
    //cocosAnalytics.onPause(true);

    if (cc.director.getScene()) {
        var name = cc.director.getScene().name;
        if (name == 'room') {
            //cc.log('停止录音');
            if (MKSceneMgr.getHandle()) {
                MKSceneMgr.getHandle().interupptRecord();
            }

        }
    }
});

cc.game.on(cc.game.EVENT_SHOW, function () {
    MKLog.log("切换到前台 判断当前场景 =", MKSceneMgr.getLoadSceneName());

});

export default class MKSceneMgr {
    private static loadSceneName: string;

    constructor() {

    }

    public static getLoadSceneName() {
        return this.loadSceneName;
    }

    public static getSceneName() {
        if (cc.director.getScene()) {
            return cc.director.getScene().name;
        }
        return "";
    }

    public static getHandle(): any {
        let ret = null;
        let scene = cc.director.getScene();
        if (scene && scene.getChildByName('Canvas')) {
            ret = scene.getChildByName('Canvas').getComponent(cc.director.getScene().name);
        }
        return ret;
    }

    public static showLoading(text: string = "", quick: boolean = false) {
        if (this.getHandle()) {
            this.getHandle().showLoading(text, quick);
        }

    }

    public static hideLoading() {
        if (this.getHandle()) {
            this.getHandle().hideLoading();
        }
    }

    public static preLoadScene() {
        cc.director.loadScene("pre_scene");
    }

    public static loadScene(sName) {
        MKLog.log("当前场景：" + sName);
        this.loadSceneName = sName;
        cc.director.loadScene(sName);
    }

    public static loadSceneNoLoading(sname) {
        let sceneHanle: any = this.getHandle();
        if (sceneHanle) {
            sceneHanle.hideLoading();
        }
        cc.director.loadScene(sname);
    }

    public static setCurSceneFrameRate(fps: number) {
        cc.game.setFrameRate(fps);
    }
}