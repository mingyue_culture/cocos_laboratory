/**
 * 线程任务管理器
 * Author: lingfeng
 * Date: 2018.12.23
 */

const { ccclass, property } = cc._decorator;

@ccclass
export default class MKTaskManager extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

    }

    start() {

    }

    // update (dt) {}

    public static checkCache() {
        let resCache = cc.loader._cache;

        console.log("[]å  = ", resCache, ", size = ", resCache.size);
    }
}
