// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var SearchTypeEnum = cc.Enum({
    关闭: -2,
    无灯: -1,
    划过: 0,
    锁定: 1,
})
module.exports = SearchTypeEnum;
