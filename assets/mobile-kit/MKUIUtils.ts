import { isPlatWechatGame, setRemoteImage } from "./MKSystem";
import MKLog from "./MKLog";

/**
 * @ UI相关-通用方法
 */

export default class MKUIUtils {

    /**
     * 设置图片
     * @param spr           精灵 
     * @param resUrl        图片资源路径
     * @param classInfo     类信息（用于打印日志信息，以便追踪错误根源）
     * @param callBack      回调函数
     */
    public static setTexture(spr: cc.Sprite, resUrl: string, classInfo: string = '', callBack?: Function) {
        cc.loader.loadRes(resUrl, cc.SpriteFrame, (err, sp) => {
            if (err) {
                MKLog.error('[MKUIUtils] setTexture classInfo = ', classInfo, ' resUrl = ', resUrl, '  err = ', err);
                return;
            }
            if (spr && spr.isValid) {
                spr.spriteFrame = sp;
                if (callBack) {
                    callBack();
                }
            }
        });
    }

    /**
     * 设置骨骼
     * @param ske           骨骼
     * @param skeUrl        资源路径
     * @param classInfo     类信息（用于打印日志信息，以便追踪错误根源）
     */
    public static setSkeleton(ske: sp.Skeleton, skeUrl: string, classInfo: string = '') {
        cc.loader.loadRes(skeUrl, sp.SkeletonData, (err, sp) => {
            if (err) {
                MKLog.log('classInfo = ', classInfo, ' skeUrl = ', skeUrl, '  err = ', err);
                return;
            };
            if (ske && ske.isValid) {
                ske.skeletonData = sp;
            }
        });
    }

    /**
     * 设置骨骼并添加动画
     * @param ske           骨骼
     * @param skeUrl        资源路径
     * @param classInfo     类信息（用于打印日志信息，以便追踪错误根源）
     */
    public static setSkeletonWithAction(ske: sp.Skeleton, skeUrl: string, actionName: string, loop: boolean, classInfo: string = '') {
        cc.loader.loadRes(skeUrl, sp.SkeletonData, (err, sp) => {
            if (err) {
                MKLog.log('classInfo = ', classInfo, ' skeUrl = ', skeUrl, '  err = ', err);
                return;
            };
            if (ske && ske.isValid) {
                ske.skeletonData = sp;
                ske.setAnimation(0, actionName, loop);
            }
        });
    }

    /**
     * 通过链接设置精灵图片
     * @param avatarUrl     远程链接地址
     * @param sprite        精灵
     */
    public static setTextureByRemoteUrl(avatarUrl: string, sprite: cc.Sprite) {
        MKLog.log('avatarUrl = ' + avatarUrl);
        // avatarUrl.indexOf('res.zjsdgm.com') != -1 含有    == -1 不含有
        if (avatarUrl && avatarUrl.indexOf('res.zjsdgm.com') != -1) {
            cc.loader.load(avatarUrl, (err, texture) => {
                if (err) {
                    MKLog.log('setTextureByRemoteUrl err = ', err);
                    return;
                }

                if (texture && cc.isValid(sprite)) {
                    sprite.spriteFrame = new cc.SpriteFrame(texture);
                } else {
                    MKLog.log('texture = null !!! or sprite is valid');
                }
            });
        }
        else if (isPlatWechatGame()) {
            if (!avatarUrl) {
                MKLog.log("[MKUIUtils] setTextureByRemoteUrl wx avatarUrl is null");
                return;
            }

            try {
                let image = window['wx'].createImage();
                image.onload = () => {
                    try {
                        let texture = new cc.Texture2D();
                        texture.initWithElement(image);
                        texture.handleLoadedTexture();
                        sprite.spriteFrame = new cc.SpriteFrame(texture);
                    } catch (e) {
                        MKLog.log('setTextureByRemoteUrl e1 = ', e);
                    }
                };
                image.src = avatarUrl;
            } catch (e) {
                MKLog.log('setTextureByRemoteUrl e2 = ', e);
            }
        } else {

        }
    }

    /**
     * 动态设置一个按钮的普通状态
     */
    public static setTextureBtnNormal(btn: cc.Button, resUrl: string, classInfo: string = '', callBack?: Function) {
        cc.loader.loadRes(resUrl, cc.SpriteFrame, (err, spriteFrame) => {
            if (err) {
                MKLog.log("setTextureBtnNormal =", err);
                return;
            }

            if (btn && cc.isValid(btn)) {
                btn.normalSprite = spriteFrame;
            }
        });
    }

    /**
     * 动态设置一个按钮的按下状态
     */
    public static setTextureBtnPress(btn: cc.Button, resUrl: string, classInfo: string = '', callBack?: Function) {
        cc.loader.loadRes(resUrl, cc.SpriteFrame, (err, spriteFrame) => {
            if (err) {
                MKLog.log("setTextureBtnNormal =", err);
                return;
            }

            if (btn && cc.isValid(btn)) {
                btn.pressedSprite = spriteFrame;
            }
        });
    }

    /**
     * 动态设置一个按钮的悬浮状态
     */
    public static setTextureBtnHover(btn: cc.Button, resUrl: string, classInfo: string = '', callBack?: Function) {
        cc.loader.loadRes(resUrl, cc.SpriteFrame, (err, spriteFrame) => {
            if (err) {
                MKLog.log("setTextureBtnNormal =", err);
                return;
            }

            if (btn && cc.isValid(btn)) {
                btn.hoverSprite = spriteFrame;
            }
        });
    }
}