# cocos_laboratory

#### 介绍
基于CocosCreator教育领域需求,积累的高可用性组件/模块仓库

#### 软件架构
* 开箱即用的高阶通用功能组件
构成:
    - 预制件
    - 内部逻辑脚本
    - 可替换的默认资源
* 高度抽象的逻辑模块


#### 安装教程
[https://www.cocos.com/products#CocosCreator](https://www.cocos.com/products#CocosCreator)

#### 使用说明
当前项目基本版本2.4.4

#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

