/**
 * 网络消息定义
 */

export default class NetworkConstants {
    public constructor() {

    }

    /********** 网关相关 ***********/
    public static heart_beat_req = 0;  // 心跳包..
    public static heart_beat_ack = 1;  // 心跳包回复
    public static user_login_req = 10; // 登陆
    public static user_login_success_ack = 11; // 登陆成功
    public static user_login_fail_ack = 12; // 登陆失败
    public static socket_gate_error = 20; // 网关登录错误信息
    public static get_seed_req = 30; // socket通信加密使用
    public static get_seed_ack = 31; // socket通信加密使用
    public static choose_game_req = 41; // 选择游戏
    public static choose_game_ack = 42; // 选择游戏回复
    public static choose_game_server_req = 43; // 选择游戏服务区
    public static choose_game_server_ack = 44; // 选择游戏服务区回复

    public static re_conn = 45; // TODO 重连
    public static re_conn_ack = 46; // 重连 返回
    public static remote_login_ack = 47; // 异地被登录

    /********** 游戏相关 ***********/
    public static sync_user_info_req = 1001; // 用户信息同步 请求
    public static sync_user_info_res = 1002; // 用户信息同步 返回


    /********** 错误信息 ***********/
    public static game_error_notice = 2000; // 所有的错误信息

    /********** 服务通知 ***********/
    public static currency_notice = 3000; // 是否有新邮件通知 主动推送

    /********** 活动系列 ***********/
    public static get_sign_info_req = 4001; // 签到情况 请求
    public static get_sign_info_res = 4002; // 签到情况 返回

}

/**
 * 输出对应关系
 * 消息号对应函数体
 */
export var NetCodeFuncMap = {
    /********** 网关相关 ***********/
    0: "S_heart_beat",
    1: "S_heart_beat",
    10: "S_user_login_info_req",
    11: "S_user_login_info_ack",
    12: "S_error_info",
    20: "S_error_info",
    43: "S_choose_game_server_req",
    44: "S_choose_game_server_ack",
    45: 'S_re_conn_req',
    46: 'S_re_conn_ack',
    47: "S_error_info",

    1001: "S_game_data_bytes",
    1002: "S_game_data_bytes",

    2000: "S_game_data_bytes",

    3000: "S_game_data_bytes",

    4001: "S_game_data_bytes",
    4002: "S_game_data_bytes",

}
