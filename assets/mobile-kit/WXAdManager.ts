import MKUtils from "./MKUtils";

/**
 * 微信激励广告
 */

// 创建视频广告
export function getVideoAd() {
    const videoAd = window['wx'].createRewardedVideoAd({ adUnitId: 'adunit-11a7287b3e518c7f' });
    return videoAd;
}

// 创建激励视频广告组件
export function showVideoAd(onShow?, onClose?, onError?, onCloseBefore?) {
    var videoID = this.getVideoAd();
    console.log('showVideoAd::::', videoID);

    videoID.load().then(() => {
        console.log('[AD] show video ID');
        videoID.show();
        MKUtils.isFun(onShow) && onShow();
    }).catch(err => {
        console.log('[AD] show video error res = ' + err);
    });

    videoID.onClose(res => {
        // 用户点击了【关闭广告】按钮
        // 小于 2.1.0 的基础库版本，res 是一个 undefined
        console.log('[AD] onClose', JSON.stringify(res));
        if (res && res.isEnded) {
            // 正常播放结束，可以下发游戏奖励
            console.log('[AD] onClose = 下发游戏奖励');
            MKUtils.isFun(onClose) && onClose(res);
        } else {
            console.log('[AD] onClose = 中途退出，不下发游戏奖励');
            MKUtils.isFun(onCloseBefore) && onCloseBefore(res);
            // 播放中途退出，不下发游戏奖励
        }
        videoID.offClose()
        videoID.offError()
    });

    videoID.onError(err => {
        console.log('[AD] onError res = ' + err);
        MKUtils.isFun(onError) && onError(err);
    });
}

// 微信公众平台---复制代码 
// {
//     let videoAd = wx.createRewardedVideoAd({
//         adUnitId: 'adunit-11a7287b3e518c7f'
//     })

//     videoAd.load()
//         .then(() => videoAd.show())
//         .catch(err => console.log(err.errMsg))
// }