
/**
 * @ Log information
 */

export default class MKLog {
    public static debug(message?: any, ...optionalParams: any[]) {
        if (MKLog.isOutputLog()) {
            console.debug(message, ...optionalParams);
        }
    }

    // 提示性日志输出
    public static info(message?: any, ...optionalParams: any[]) {
        //if (MKLog.isOutputLog()) {
        console.info(message, ...optionalParams);
        //}
    }

    public static log(message?: any, ...optionalParams: any[]) {
        if (MKLog.isOutputLog()) {
            console.log(message, ...optionalParams);
        }
    }

    public static warn(message?: any, ...optionalParams: any[]) {
        if (MKLog.isOutputLog()) {
            console.warn(message, ...optionalParams);
        }
    }

    public static error(message?: any, ...optionalParams: any[]) {
        //if (MKLog.isOutputLog()) {
        console.error(message, ...optionalParams);
        //}
    }

    public static cccDebug(message?: any, ...optionalParams: any[]) {
        if (CC_DEBUG) {
            console.log(message, ...optionalParams);
        }
    }

    // 是否输出日志信息 true:输出  false:不输出
    public static isOutputLog(): boolean {
        return true;
    }

    getDateString() {
        var d = new Date();
        var str = String(d.getHours());
        var timeStr = "";
        timeStr += (str.length == 1 ? "0" + str : str) + ":";
        str = String(d.getMinutes());
        timeStr += (str.length == 1 ? "0" + str : str) + ":";
        str = String(d.getSeconds());
        timeStr += (str.length == 1 ? "0" + str : str) + ":";
        str = String(d.getMilliseconds());
        if (str.length == 1) str = "00" + str;
        if (str.length == 2) str = "0" + str;
        timeStr += str;

        timeStr = "[" + timeStr + "]";
        return timeStr;
    };
}