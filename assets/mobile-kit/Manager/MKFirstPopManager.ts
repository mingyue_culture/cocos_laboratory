

/**
 * 首次弹出界面管理
 * Author: lingfeng
 * Date: 2019.01.14
 */

export const NoticeShowFirstPop = "NoticeShowFirstPop";

export class ShowFirstPop {
    ID: any;
    Func: Function;
    ShowCountIndex: number
};

const { ccclass, property } = cc._decorator;

@ccclass
export default class MKFirstPopManager {

    public static firstPopAction: Array<ShowFirstPop> = [];

    public static onShowNextPop(node) {
        if (this.firstPopAction != null && this.firstPopAction.length > 0) {
            var func = this.firstPopAction.shift();
            if (func && func.Func) {
                func.Func.call(node);
            }
        } else {
            // DataManager.I.globalData.isFirstInHall = false;
        }
    }
}
