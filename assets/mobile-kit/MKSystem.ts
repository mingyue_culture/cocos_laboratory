/**
 * Author: ling_feng
 * Date: 2018.5.17
 * CopyRight:
 * 系统底层类
 */

import { httpGet, httpPost } from './HttpMgr';
import MKLog from './MKLog';

/**
 * 获取平台
 */
export function isNative() {
    return (cc.sys.isNative) && (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID);
}

export function isOsIOS() {
    return (cc.sys.os == cc.sys.OS_IOS);
}

export function isOsAndroid() {
    return (cc.sys.os == cc.sys.OS_ANDROID);
}

export function isPlatWechatGame() {
    return (cc.sys.platform == cc.sys.WECHAT_GAME);
}

export function isPlatTouTiaoGame() {
    return false;
}

export function getPlatType(): string {
    if (isOsAndroid()) {
        return "ANDROID";
    }
    else if (isOsIOS()) {
        return "IOS";
    }
    else if (cc.sys.isBrowser) {
        return "H5";
    }
    else {
        return "";
    }
}

export function getSourceType(): string {
    if (isPlatTouTiaoGame()) {
        return "tt";
    }

    if (isPlatWechatGame()) {
        return "wx"
    }

    return "h5"
}

/**
 * 获取版本
 */
export function getApkVersionCode() {
    return jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "getVersionCode", "()I");
}

export function getIpaVersion() {
    return jsb.reflection.callStaticMethod("reflectClass", "getVersion")
}

export function getApkVersion() {
    return jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "getVersionName", "()Ljava/lang/String;");
}

export function isWifi() {
    if (isNative()) {
        if (cc.sys.os === cc.sys.OS_ANDROID) {
            return jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "isWifi", "()Z");
        } else
            return !jsb.reflection.callStaticMethod("reflectClass", "isWifi");
    }

    return true;
}

export function getDeviceID() {
    if (isOsIOS() && isNative())
        return jsb.reflection.callStaticMethod("reflectClass", "getDeviceId");
    else
        return jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "getMac", "()Ljava/lang/String;");
}

export function endGame() {
    if (isNative()) {
        if (cc.sys.os === cc.sys.OS_ANDROID) {
            cc.game.end();
        } else
            jsb.reflection.callStaticMethod("reflectClass", "close");
    } else {
        cc.game.end();
    }
}

export function getBattery() {
    if (isOsIOS() && isNative()) {
        return jsb.reflection.callStaticMethod("reflectClass", "getBattery") * 100;
    }
    else {
        return jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "getBattery", "()Ljava/lang/String;");
    }
}

/**
 * 根据精度维护取距离
 * @param lon1 
 * @param lat1 
 * @param lon2 
 * @param lat2 
 */
export function getMapDistance(lon1, lat1, lon2, lat2) {
    let radlon1 = (Math.PI / 180) * lon1;
    let radlon2 = (Math.PI / 180) * lon2;
    let radlat1 = (Math.PI / 180) * lat1;
    let radlat2 = (Math.PI / 180) * lat2;

    let earthR = 6371;
    let distance = Math.acos(Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radlon2 - radlon1)) * earthR;

    return distance;
}

/**
 * 获取远程图片
 * @param image         需要设置图片的精灵
 * @param url           链接地址
 * @param imgType       'jpg' / 'png'
 */
export function setRemoteImage(image: cc.Sprite, url: string, imgType: string) {
    MKLog.log("load img start #############");
    let imgUrl = url;
    if (!isNative() && !isPlatWechatGame()) {
        imgUrl = encodeURIComponent(imgUrl)
    }
    cc.loader.load({
        url: imgUrl,
        type: imgType
    }, function (err, tex) {
        if (err) {
            MKLog.log("load img erro #############");
            return;
        }
        if (!image)
            return;

        var frame = new cc.SpriteFrame(tex);
        image.spriteFrame = frame;
    });
}
