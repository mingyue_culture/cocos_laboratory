
import MKLog from "./MKLog";
import { isPlatWechatGame, isOsAndroid, isOsIOS } from "./MKSystem";
import MKDataReport from "./MKDataReport";
import MKEventDispatch from "./MKEventDispatch";
import MKUtils from "./MKUtils";


/**
 * Author: ling_feng
 * Date: 2018.5.22
 * CopyRight:
 * 微信接口交互
 */

/**
 * 获取微信接口
 */
export function getWindowWX() {
    return window['wx'];
}

/**
 * 微信请求接口
 * @param {*} url 地址
 * @param {*} data 数据
 * @param {*} method 模式 GET | POST
 * @param {*} callback 回调
 */
export function sendWxRequest(url, data, method, callback) {
    window['wx'].request({
        url: url,
        data: data,
        header: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        method: method,

        success(res) {
            MKLog.log("发送wx requests成功 解析res = " + JSON.stringify(res));
            callback();
        },
        fail(res) {
            MKLog.log("发送失败");
        },
        complete(res) {
            MKLog.log("发送完成");
        }
    });
}

/**
 * 获取微信登陆凭证
 */

export function getWxLogin(callback) {
    //调用登录接口，获取 code
    window['wx'].login({
        success(res) {
            MKLog.log("获取登陆凭证 getWxLogin = " + JSON.stringify(res));
            callback(res.code);
        }
    });
}

/**
 * 获取微信用户信息
 */

export function getWxUserInfo(callback) {
    window['wx'].getUserInfo({
        success(res) {
            MKLog.log('getUserInfo data = ' + JSON.stringify(res));
            MKLog.log('解密后 data 格式化Json = ' + JSON.stringify(res.userInfo));

            callback(res.userInfo)
        },
        fail(res) {
            MKLog.log("[WxApi] 获取个人信息失败");
            callback(-1)
        }
    });
}

/**
 * 微信登陆
 */
// export function getWxUserInfo(callback){
//     //调用登录接口，获取 code
//     wx.login({
//         success: function (res) {
//             MKLog.log("成功拿到code  解析res = " + JSON.stringify(res));
//             var testObject = {
//                 code:res.code
//             };

//             //发起网络请求
//             var data = {
//                 cmd:"Login",
//                 p:JSON.stringify(testObject)
//             };
//             sendWxRequest('http://192.168.31.235:8340', data, 'POST', ()=>{
//                 wx.getUserInfo({
//                     success: function (res) {
//                         MKLog.log('getUserInfo data = ' + JSON.stringify(res));
//                         MKLog.log('解密后 data 格式化Json = ' + JSON.stringify(res.userInfo));

//                         callback(res.userInfo)
//                     }
//                 })
//             });
//         }
//     })
// }

/**
 * 检测当前用户登录态
 */
export function checkSessionValid(callback: Function) {
    window['wx'].checkSession({
        success() {
            //session 未过期，并且在本生命周期一直有效
            // wx.getUserInfo({
            //     success: function (res) {
            //         MKLog.log('getUserInfo data = ' + JSON.stringify(res));
            //         MKLog.log('解密后 data 格式化Json = ' + JSON.stringify(res.userInfo));

            //         callback(res.userInfo)
            //     }
            // })

            MKLog.log("验证 sesion有效");
            callback(true);
        },
        fail() {
            //登录态过期
            // getWxUserInfo(callback);

            MKLog.log("验证 sesion已过期");
            callback(false);
        }
    });
}

/**
 * 显示转发按钮
 */
export function getShowShareMenu() {
    return window['wx'].showShareMenu();
}

/**
 * 隐藏转发按钮
 */
export function getHideShareMenu() {
    return window['wx'].hideShareMenu();
}

/**
 * 修改转发属性
 * 参考链接：https://developers.weixin.qq.com/minigame/dev/tutorial/open-ability/share.html
 */
export function updateShareTicket(isUse: boolean) {
    window['wx'].updateShareMenu({
        withShareTicket: false
    })
}


/**
 * 监听转发回调
 * @param {*} title 转发标题
 * @param {*} query 转发参数
 */
export function onShareApp(title: string, query: { serverID: number, uid: string }, userInfo: string, config: string[]) {
    console.log("[WxApi] onShareApp [监听转发参数 ==== ]", query);
    window['wx'].onShareAppMessage(function () {
        //引用阿拉丁参数
        //window['wx'].aldOnShareAppMessage(function () {
        MKLog.log("用户点击了“转发”按钮");

        let imageID = 1;
        let imageUrl = "https://res.zjsdgm.com/sanguo/share/wx_share_01.jpg";
        MKLog.log("本地内存读取到的数据 =", config);
        if (config) {
            let obKey = Object.keys(config);
            let randomIndex = Math.floor(Math.random() * obKey.length);
            MKLog.log("随机到的下标 =", randomIndex);
            let key = obKey[randomIndex];
            let ShareInfo = config[key];

            MKLog.log("线上配置分享文本条目数量 =", obKey, ", 使用的图片下表 =", key, ", 分享信息的数据结构 =", ShareInfo);
            title = ShareInfo.des;
            imageUrl = ShareInfo.image;
            imageID = ShareInfo.mageID;
        }

        // 记录分享所使用的图片
        MKDataReport.aladdinEvent('分享', { '分享图片Index': imageID, '位置': "三个点" });

        return {
            title: title,
            imageUrl: imageUrl,
            query: "serverID=" + query.serverID + "&uid=" + query.uid + "&shareImageID=" + imageID
        }
    });
}

/**
 * 游戏内主动转发接口
 * @param {*} title 转发标题
 * @param {*} isCutRes 转发大图是截取当前屏幕还是默认的配图
 * @param {*} query 转发参数
 */
export function sendShare(title: string, isCutRes: boolean, query: { serverID: number, uid: string }, pos: string = "", userInfo: string, config: string[]) {
    MKLog.log("[WxApi] sendShare 用户发起了主动转发的请求");

    if (cc.sys.isBrowser) {
        MKLog.log("浏览器环境");
        return;
    }

    let image = "https://res.zjsdgm.com/sanguo/share/wx_share_01.jpg";
    let imageID = 1;
    MKLog.log("[WxApi] sendShare 本地内存读取到的数据 =", config, ", isCutRes = ", isCutRes);
    if (config && !isCutRes) {
        MKLog.log("[WxApi] sendShare 测试 = 001");
        let obKey = Object.keys(config);
        MKLog.log("[WxApi] sendShare 测试 = 002", obKey);
        let randomIndex = Math.floor(Math.random() * obKey.length);
        MKLog.log("[WxApi] sendShare 随机到的下标 =", randomIndex);
        let key = obKey[randomIndex];
        let ShareInfo = config[key];
        MKLog.log("[WxApi] sendShare 线上配置分享文本条目数量 =", obKey, ", 使用的图片下表 =", key, ", 分享信息的数据结构 =", ShareInfo);
        title = ShareInfo.des;
        image = ShareInfo.image;
        imageID = ShareInfo.mageID;
    }

    MKDataReport.aladdinEvent('分享图片统计', { '分享图片Index': imageID, '位置': pos });

    if (isCutRes) {
        MKLog.log('[WxApi] sendShare 是否剪切图片 =', isCutRes);

        // @ts-ignore
        canvas.toTempFilePath({
            // @ts-ignore
            x: canvas.width / 2 - 500,
            // @ts-ignore
            y: canvas.height / 2 - 400,
            width: 1000,
            height: 800,
            destWidth: 500,
            destHeight: 400,
            success: (res) => {
                let path = res.tempFilePath
                MKLog.log('[WxApi] sendShare 截图成功 图片路径为 path =', path);
                window['wx'].shareAppMessage({
                    //引用阿拉丁参数
                    //window['wx'].aldShareAppMessage({
                    title: title,
                    imageUrl: path,
                    query: "serverID=" + query.serverID + "&uid=" + query.uid + "&shareImageID=" + imageID
                });
            },
            fail: () => {
                MKLog.log("[WxApi] sendShare 截屏失败 使用默认配置图片");

                let obKey = Object.keys(config);
                let randomIndex = Math.floor(Math.random() * obKey.length);
                let key = obKey[randomIndex];
                let ShareInfo = config[key];
                title = ShareInfo.des;
                image = ShareInfo.image;
                imageID = ShareInfo.mageID;

                window['wx'].shareAppMessage({
                    //引用阿拉丁参数
                    //window['wx'].aldShareAppMessage({
                    title: title,
                    imageUrl: image,
                    query: "serverID=" + query.serverID + "&uid=" + query.uid + "&shareImageID=" + imageID
                });
            },
            complete: () => {
                MKLog.log("[WxApi] sendShare 截屏完成");
            }
        });
    }
    else {
        window['wx'].shareAppMessage({
            title: title,
            imageUrl: image,
            query: "serverID=" + query.serverID + "&uid=" + query.uid + "&shareImageID=" + imageID
        });
    }
}


/**
 * 游戏内主动转发接口
 * @param {*} title 转发标题
 * @param {*} query 转发参数
 */
export function sendShareRedPack(query: { serverID: number, uid: string }) {
    MKLog.log("用户发起了主动转发的请求");

    var shareImage = "https://res.zjsdgm.com/sanguo/share/img_hb_fx.jpg";
    var shareImageIndex = 100;

    MKDataReport.aladdinEvent('分享', { '分享图片Index': shareImageIndex, '位置': "红包门客" });

    window['wx'].shareAppMessage({
        //引用阿拉丁参数
        //window['wx'].aldShareAppMessage({
        title: "分享红包，分享快乐。",
        imageUrl: shareImage,
        query: "serverID=" + query.serverID + "&uid=" + query.uid + "&shareImageID=" + shareImageIndex
    });
}

/**
 * 监听微信更新 --- 冷启动
 */
export function ListenUpdateRes() {
    if (!isPlatWechatGame()) {
        return;
    }

    if (typeof window['wx'].getUpdateManager === 'function') {
        const updateManager = window['wx'].getUpdateManager()

        updateManager.onCheckForUpdate(function (res) {
            // 请求完新版本信息的回调
            MKLog.log("[wxApi] ListenWxUpdate 监听更新回调 = ", res.hasUpdate);
            MKEventDispatch.I.emit("ListenWxUpdate", res.hasUpdate);
        })

        updateManager.onUpdateReady(function () {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
        })

        updateManager.onUpdateFailed(function () {
            // 新的版本下载失败
        })
    }
}

interface UserInfo {
    serverId: string,
    uid: string,
    nickName: string
}

/**
 * 微信支付购买虚拟货币
 * @param price  价格   购买数量 = 总价/单价
 */
export function WeChatPay(orderID: string, price: number, extend: string, userInfo: UserInfo, successFunc?: Function, failFunc?: Function) {
    MKLog.log("[WeChatPay] extend =", extend, "price =", price, ", extend =", extend);

    if (cc.sys.isBrowser) {
        successFunc && successFunc();
        return;
    }

    let info = {
        orderid: orderID,               //cp订单号[订单号不能出现下划线]
        money: price,                   //商品价格（元）
        extend: extend,                 //扩展字段，原路返回给服务端
        server_num: userInfo.serverId,      //区服ID
        actorid: userInfo.uid,              //角色ID
        actorname: userInfo.nickName        //角色名
    }

    if (isOsAndroid()) {
        let quantity = price * 10;
        window['wx'].requestMidasPayment({
            mode: 'game',
            env: 0,     // 0:米大师正式环境     1:米大师沙箱环境
            offerId: '1450017284',
            currencyType: 'CNY',
            platform: 'android',
            buyQuantity: quantity,
            zoneId: 1,
            success() {
                // 支付成功
                MKLog.log('支付成功！！！');
            },
            fail({ errMsg, errCode }) {
                // 支付失败
                MKLog.log('支付失败！！！ errMsg = ', errMsg, ' errCode = ', errCode);
                failFunc && failFunc();
            },
            complete() {
                MKLog.log('支付完成！！！');
            }
        });
    } else {
        if (cc.sys.isBrowser) {
            successFunc && successFunc();
        }
        else {

        }
    }
}

/**
 * 获取该小程序下已保存的本地缓存文件列表
 */
export function getSavedFileList() {
    window['wx'].getSavedFileList({
        success(res) {
            MKLog.log("测试获取缓存 =", res.fileList)
        }
    });
}

/**
 * 删除本地缓存文件
 */
export function removeSavedFile() {
    window['wx'].getSavedFileList({
        success(res) {
            if (res.fileList.length > 0) {
                window['wx'].removeSavedFile({
                    filePath: res.fileList[0].filePath,
                    complete(res) {
                        MKLog.log(res)
                    }
                });
            }
        }
    });
}

/**
 * 获取文件服务列表
 */
export function getFileSystemManager() {
    return window['wx'].getFileSystemManager();
}

/**
 * 强制退出微信小游戏
 */
export function exitMiniProgram() {
    window['wx'].exitMiniProgram({
        success(res) {

        },
        fail(res) {

        },
        complete(res) {

        }
    });
}

/**
 * 日志监控
 */
export function reportMonitor(name: string, value: number) {
    window['wx'].reportMonitor(name, value);
}

/**
 * 进入客服会话。要求在用户发生过至少一次 touch 事件后才能调用。
 */
export function openCustomerServiceConversation() {
    window['wx'].openCustomerServiceConversation({
        sessionFrom: "test01",
        showMessageCard: false,
        sendMessageTitle: "111",
        sendMessagePath: "222",
        sendMessageImg: "",
        success() {
            MKLog.log("客服消息调起成功 === ok");
        },
        fail(res) {
            MKLog.log("客服消息调起失败 === err");
        },
        complete() {
            MKLog.log("客服消息调起完成 === com");
        }
    });
}

/**************** --- 屏幕相关 --- *********************/

/**
 * 保持屏幕长亮
 */
export function setKeepScreenOn() {
    window['wx'].setKeepScreenOn({
        keepScreenOn: true
    });
}

/**************** --- 网络相关 --- *********************/

/**
 * 监听网络状态变化事件
 * 
 * 参数
    Object res
    isConnected	boolean	当前是否有网络链接	
    networkType	string	网络类型
 */
export function onNetworkStatusChange() {
    window['wx'].onNetworkStatusChange(function (res) {
        console.log(res.isConnected)
        console.log(res.networkType)
    });
}

/**************** --- 屏幕旋转 --- *********************/

/**
 * 取消监听横竖屏切换事件
 */
export function offDeviceOrientationChange() {
    window['wx'].offDeviceOrientationChange(function () {
        console.log("关闭监听横竖屏");
    });
}

/**
 * 监听横竖屏切换事件
 */
export function onDeviceOrientationChange() {
    window['wx'].onDeviceOrientationChange(function () {
        console.log("开启监听横竖屏");
    });
}

/**************** --- 性能相关 --- *********************/

/**
 * 监听内存不足告警事件
 */
export function onMemoryWarning() {
    window['wx'].onMemoryWarning(function () {
        console.log('onMemoryWarningReceive')
    });
}

/**
 * 加快触发 JavaScriptCore 垃圾回收（Garbage Collection）
 */
export function triggerGC() {
    window['wx'].triggerGC();
}

/**
 * 是否打开调试日志
 * @param bool // true: 打开调试 false: 关闭调试
 */
export function setEnableDebug(bool: boolean) {
    window['wx'].setEnableDebug({
        enableDebug: bool
    });
}

/**
 * 获取设备性能
 */
export function getSystemInfo() {
    window['wx'].getSystemInfo({
        success(res) {
            console.log("[系统信息] AppData =", res);
        }
    });
}


/**************** --- 帧率相关 --- *********************/

/**
 * 可以修改渲染帧率。默认渲染帧率为 60 帧每秒。修改后，requestAnimationFrame 的回调频率会发生改变。
 */
export function setPreferredFramesPerSecond(fps: number) {
    window['wx'].setPreferredFramesPerSecond(fps);
}

/**
 * 取消由 requestAnimationFrame 添加到计划中的动画帧请求
 */
// export function cancelAnimationFrame(requestID: number){
//     window['wx'].cancelAnimationFrame(requestID);
// }

/**
 * 在下次进行重绘时执行
 */
// export function requestAnimationFrame(){
//     let id = window['wx'].requestAnimationFrame(function(){
//         // MKLog.log("[WXApi] 在下次进行重新绘制执行的ID =", id);
//     });
// }

/**************** --- 防沉迷相关 --- *********************/

/**
 * 根据用户当天游戏时间判断用户是否需要休息
 * @param todayPlayedTime 今天已经玩游戏的时间，单位：秒
 */
export function checkIsUserAdvisedToRest(todayPlayedTime: number) {
    window['wx'].checkIsUserAdvisedToRest({
        todayPlayedTime: todayPlayedTime,
        success(res) {
            MKLog.log("[WXApi] 防沉迷返回结果 =", res.result);
        },
        fail() {

        },
        complete() {

        }
    });
}

export default class WxApi {
    /**
     * 获取网络类型
     * @param res.networkType 的合法值
     * wifi	wifi 网络
     * 2g	2g 网络
     * 3g	3g 网络
     * 4g	4g 网络
     * unknown	Android 下不常见的网络类型
     * none	无网络
    */
    public static getNetworkType(callback: Function) {
        window['wx'].getNetworkType({
            success(res) {
                callback(res);
            }
        });
    }
}

/** 
 * 获取场景值
 * 参考：https://developers.weixin.qq.com/miniprogram/dev/framework/app-service/scene.html
 */
export function getEnterPathInfo() {
    const version = window['wx'].getSystemInfoSync().SDKVersion;
    console.log('版本号 = ' + version);
    let ver = '2.2.4';
    let comRes = MKUtils.compareVersion(version, ver);
    if (comRes > 0) {
        console.log('版本号 > ' + ver);
    } else if (comRes == 0) {
        console.log('版本号 == ' + ver);
    } else {
        console.log('版本号 < ' + ver);
    }

    let obj = window['wx'].getLaunchOptionsSync();
    if (obj && obj.scene === 1104) {

    }
    // console.log("[wx.getLaunchOptionsSync()] obj =", obj);
    // console.log('启动小游戏的 path = ' + obj.path);
    // console.log('启动小游戏的 scene = ' + obj.scene);
    // console.log('启动小游戏的 query = ' + obj.query);
    // console.log('启动小游戏的 shareTicket = ' + obj.shareTicket);
    // console.log('启动小游戏的 referrerInfo = ' + obj.referrerInfo);
}

// 监听从后台进入游戏场景时获取微信进入场景值
export function backGamePathInfo() {
    window['wx'].onShow((obj) => {
        if (obj && obj.scene === 1104) {
            // 1104 表示从我的小程序进入游戏
        }
        // console.log('启动小游戏的 scene = ' + obj.scene);
        // console.log('启动小游戏的 query = ' + obj.query);
        // console.log('启动小游戏的 shareTicket = ' + obj.shareTicket);
        // console.log('启动小游戏的 referrerInfo = ' + obj.referrerInfo);
    });
}



