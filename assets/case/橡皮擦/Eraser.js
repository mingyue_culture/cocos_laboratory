cc.Class({
    extends: cc.Component,

    properties: {
        cr: {
            default: 30,
            tooltip: '涂抹圆的半径'
        },
        mask: cc.Mask
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {
        let self = this;
        this.node.on(cc.Node.EventType.TOUCH_START, (event) => {
            cc.log('touch start');
            let point = event.touch.getLocation();
            point = self.mask.node.convertToNodeSpaceAR(point);
            self._addCircle(point);
        }, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, (event) => {
            let point = event.touch.getLocation();
            point = self.mask.node.convertToNodeSpaceAR(point);
            self._addCircle(point);
        }, this);

    },
    _addCircle(pos) {
        this.mask._graphics.lineWidth = 1;
        // this.mask._graphics.strokeColor = cc.color(255, 0, 0);//这个也没啥效果
        // this.mask._graphics.fillColor = cc.color(100, 120, 2);//这个没什么效果
        this.mask._graphics.circle(pos.x, pos.y, this.cr);
        this.mask._graphics.fill();
        this.mask._graphics.stroke();
    }

    // update (dt) {},
});
