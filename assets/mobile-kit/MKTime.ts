/**
 * Author: ling_feng
 * Date: 2018.5.16
 * CopyRight:
 * 时间管理类
 */

export default class MKTime {
    private static pad(num: number, n: number) {
        var y = '0000000000000000000000000000' + num; //爱几个0就几个，自己够用就行
        return y.substr(y.length - n);
    }

    /**
     * 获取时间戳：返回距 1970 年 1 月 1 日之间的毫秒数
     */
    public static getMSeconds() {
        var dt = new Date();
        return dt.getTime()
    }

    /**
     * 格式化时间和分钟
     * @param timestamp 可支持传入时间 
     */
    public static formatHM(timestamp) {
        var date = new Date();
        if (timestamp)
            date.setTime(timestamp * 1000);
        var seperator2 = ":";
        var currentdate = this.pad(date.getHours(), 2) + seperator2 + this.pad(date.getMinutes(), 2);
        return currentdate;
    }

    public static getSeconds() {
        return Math.round(this.getMSeconds() / 1000);
    }

    /**
     * xx时xx分xx秒
     * Commit: 注意时间会截取成两位
     * @param sec 
     */
    public static formatTime(sec: number) {
        var h = Math.floor(sec / 3600);
        var m = Math.floor((sec - 3600 * h) / 60);
        var s = Math.ceil(sec % 60);
        return ('0' + h).substring(('0' + h).length - 2) + ":" + ('0' + m).substring(('0' + m).length - 2) + ":" + ('0' + s).substring(('0' + s).length - 2)
    }

    /**
     * xx小时xx分xx秒
     * @param sec 
     */
    public static formatTimeAsciiSc(sec: number) {
        var h = Math.floor(sec / 3600);
        var m = Math.floor((sec - 3600 * h) / 60);
        var s = Math.ceil(sec % 60);

        return h + "小时" + ('0' + m).substring(('0' + m).length - 2) + "分" + ('0' + s).substring(('0' + s).length - 2) + "秒"
    }

    /**
     * xx天xx小时xx分xx秒
     * @param sec 
     */
    public static formatTimeDDHHMMSS(sec: number) {
        let h = Math.floor(sec / 3600);
        let m = Math.floor((sec - 3600 * h) / 60);
        let s = Math.ceil(sec % 60);
        let d = Math.floor(h / 24);
        h = h - d * 24;
        let ft = '';
        if (d > 0) {
            ft = d + '天' + h + "小时" + ('0' + m).substring(('0' + m).length - 2) + "分" + ('0' + s).substring(('0' + s).length - 2) + "秒";
        } else {
            ft = h + "小时" + ('0' + m).substring(('0' + m).length - 2) + "分" + ('0' + s).substring(('0' + s).length - 2) + "秒";
        }
        return ft;
    }

    /**
     * 分秒
     * @param sec 
     */
    public static formatTimeMS(sec: number) {
        var h = Math.floor(sec / 3600);
        var m = Math.floor((sec - 3600 * h) / 60);
        var s = Math.floor(sec % 60);
        return ('0' + m).substring(('0' + m).length - 2) + ":" + ('0' + s).substring(('0' + s).length - 2)
    }

    //1990-02-04 12:22:44
    public static getNowFormatDate(timestamp) {
        var date = new Date();
        if (timestamp)
            date.setTime(timestamp * 1000);

        var seperator1 = "-";
        var seperator2 = ":";
        var month: any = date.getMonth() + 1;
        var strDate: any = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate +
            " " + this.pad(date.getHours(), 2) + seperator2 + this.pad(date.getMinutes(), 2) +
            seperator2 + this.pad(date.getSeconds(), 2);
        return currentdate;
    }

    //02-04 12:22
    public static getTimeFormatDate(timestamp) {
        var date = new Date();
        if (timestamp)
            date.setTime(timestamp * 1000);

        var seperator1 = "-";
        var seperator2 = ":";
        var month: any = date.getMonth() + 1;
        var strDate: any = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = month + seperator1 + strDate +
            " " + this.pad(date.getHours(), 2) + seperator2 + this.pad(date.getMinutes(), 2);
        return currentdate;
    }

    // 20180612
    public static getDateYYYYMMDD() {
        var date = new Date();
        var month: any = date.getMonth() + 1;
        var strDate: any = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var yyyymmdd = String(date.getFullYear()) + String(month) + String(strDate);
        return yyyymmdd;
    }

    public static getTimeFormat(timestamp) {
        var date = new Date(timestamp);
        var month: any = date.getMonth() + 1;
        var strDate: any = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var yyyymmdd = String(date.getFullYear()) + '.' + String(month) + "." + String(strDate);
        return yyyymmdd;
    }

    /**
     * 返回月日，如果时间是今天就返回“今天”，如果是昨天返回“昨天”，超过昨天返回“月-日”
     */
    public static getDateMMDDOrStr(timestamp) {
        var date = new Date();
        if (timestamp)
            date.setTime(timestamp * 1000);

        var seperator1 = "-";
        var month: any = date.getMonth() + 1;
        var strDate: any = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = month + seperator1 + strDate;

        //判断时间戳是今天还是昨天

        return currentdate;
    }
}