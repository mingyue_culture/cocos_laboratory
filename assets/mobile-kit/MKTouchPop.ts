/**
 * 点击弹出效果
 */

const { ccclass, property } = cc._decorator;

@ccclass
export default class MKTouchPop extends cc.Component {
    onLoad() {
        this.node.scale = 0.5;
        this.node.opacity = 0;
        var seq = cc.sequence(
            cc.spawn(cc.scaleTo(0.1, 1.1), cc.fadeTo(0.1, 255)),
            cc.scaleTo(0.1, 1.0)
        );
        this.node.runAction(seq);
    }
}
