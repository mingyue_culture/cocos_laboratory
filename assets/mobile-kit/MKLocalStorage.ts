

/**
 * @ 本地存储
 */

export const serverId: number = 1000;

function getServerIDKey(k: string) {
    return serverId + k;
}

/**
* 保存单个数据
* @param k 
* @param v 
*/
export function saveItem(k: string, v: any) {
    k = getServerIDKey(k);
    cc.sys.localStorage.setItem(k, v);
}

/**
 * 获取保存数据
 * @param k  
 */
export function getItem(k: string) {
    k = getServerIDKey(k);
    return cc.sys.localStorage.getItem(k);
}

/**
 * 保存复杂数据 （结构体类型 or others）
 * @param k 
 * @param v 
 */
export function saveItems(k: string, v: any) {
    k = getServerIDKey(k);
    cc.sys.localStorage.setItem(k, JSON.stringify(v));
}

/**
 * 获取复杂数据 （结构体类型 or others）
 * @param k 
 */
export function getItems(k: string) {
    k = getServerIDKey(k);
    if (cc.sys.localStorage.getItem(k) != "") {
        return JSON.parse(cc.sys.localStorage.getItem(k));
    }
    else {
        return null;
    }
}

/**
 * 删除k值所对应的数据（包括k值）
 * @param k 
 */
export function removeItem(k: string) {
    k = getServerIDKey(k);
    cc.sys.localStorage.removeItem(k);
}