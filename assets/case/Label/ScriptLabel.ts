/**
 * 文本渲染的事项
 */

const { ccclass, property } = cc._decorator;

@ccclass
export default class ScriptLabel extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property(cc.Label)
    label2: cc.Label = null;

    @property
    text: string = 'hello';

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    // update(dt) { }

    start() {
        this.label.node.color = cc.color(255, 0, 0);        // 正确的写法

        // @ts-ignore
        this.label._forceUpdateRenderData();
        console.log("宽度刷新 --- 01 ---", this.label.node.width);

        // 即时刷新文本长度 02
        this.scheduleOnce(() => {
            console.log("宽度刷新 --- 02 ---", this.label.node.width);
        }, 0.1);
    }

    /**
     * 16进制色值
     */
    onFromHEX() {
        var color = cc.Color.BLACK;
        color.fromHEX("#FFFF33"); // Color {r: 255, g: 255, b: 51, a: 255};

        this.label2.node.color = color;
    }
}
