import MKLog from "./MKLog";

/**
 * 点击间隔
 */

const { ccclass, property } = cc._decorator;

@ccclass
export default class MKTouchSpace extends cc.Component {

    btn: cc.Button = null;

    onLoad() {
        this.btn = this.node.getComponent(cc.Button);
    }

    onTouchSpace() {
        MKLog.log("[点击态测试] MKTouchSpace onTouchSpace");
        this.btn.enabled = false;
        var self = this;
        this.scheduleOnce(function () {
            self.btn.enabled = true;
        }, 0.8);
    }
}
