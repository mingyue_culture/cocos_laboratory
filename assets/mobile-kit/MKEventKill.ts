/**
 * Comment:这个组件用来点击组件外关闭组件
 */

const { ccclass, property } = cc._decorator;

@ccclass
export default class MKEventKill extends cc.Component {

    onLoad() {
        // var self = this;
        // this.node.on(cc.Node.EventType.TOUCH_START, function (event: cc.Event.EventTouch) {
        //     let rect = self.node.children[0].getBoundingBoxToWorld();
        //     if (self.node.childrenCount > 0 && rect.contains(event.getLocation())) {
        //         //cc.log("点击在组件内")
        //     } else {
        //         //cc.log("点击组件外的操作");
        //         self.node.destroy()
        //     }
        //     event.stopPropagation();
        // });
        this.onEventListener();
    }

    onEventListener() {
        var self = this;
        function onTouchStart(event) {
            
        }

        function onTouchMove(event) {

        }

        function onTouchEnd(event) {
            self.node.destroy();
        }

        function onTouchCancel(event) {
        }

        this.node.on(cc.Node.EventType.TOUCH_START, onTouchStart, this.node);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, onTouchMove, this.node);
        this.node.on(cc.Node.EventType.TOUCH_END, onTouchEnd, this.node);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, onTouchCancel, this.node);
    }
};
