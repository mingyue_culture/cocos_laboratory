/**
 * Author: ling_feng
 * Date: 2018.5.16
 * CopyRight:
 * 通用工具类
 */

import { isNative } from "./MKSystem";

export default class MKUtils {

    /**
     * 克隆节点
     * @param obj 
     */
    public static clone(obj) {
        var self = this;
        var o: any = {};
        if (typeof obj == "object") {
            if (obj === null) {
                o = null;
            } else {
                if (obj instanceof Array) {
                    o = [];
                    for (var i = 0, len = obj.length; i < len; i++) {
                        o.push(self.clone(obj[i]));
                    }
                } else {
                    o = {};
                    for (var j in obj) {
                        o[j] = self.clone(obj[j]);
                    }
                }
            }
        } else {
            o = obj;
        }
        return o;
    }

    public static copyIconToSDCard() {
        if (!isNative()) return;

        //cc.log(jsb.fileUtils.getWritablePath());
        var data = jsb.fileUtils.getDataFromFile(cc.url.raw('resources/special/icon.jpg'));
        jsb.fileUtils.writeDataToFile(data, jsb.fileUtils.getWritablePath() + 'icon.jpg')
        //cc.log("copyIconToSDCard")
    }

    public static shortName(name: string) {
        if (name.length <= 12)
            return name;

        return name.substr(0, 11) + '...';
    }

    /**
     * 名字最大长度截取
     */
    public static nameMaxCut(input, maxLen) {
        var output = "";
        var strlen = 0;
        for (var i = 0; i < input.length; i++) {
            var ch = input[i];
            if (ch.charCodeAt() > 255) //如果是汉字，则字符串长度加2
            {
                strlen += 2;
            }
            else {
                strlen++;
            }

            if (strlen <= maxLen) {
                output += ch;
            }
        }

        if (strlen > maxLen) {
            return output += "..";
        }

        return output;
    }

    public static isFun(fun) {
        return (fun && typeof fun === 'function');
    }

    public static isArray(obj) {
        return Object.prototype.toString.call(obj) === '[object Array]';
    }

    public static getCardValueByServerData(d) {
        return ((d[0] - 1) * 10 + d[1])
    }

    public static getServerCardVarlue(cid) {
        return [Math.floor(cid / 10) + 1, cid % 10];
    }

    private static cutString(pStr, pLen) {

        // 原字符串长度 
        var _strLen = pStr.length;
        var _tmpCode;
        var _cutString;
        // 默认情况下，返回的字符串是原字符串的一部分 
        var _cutFlag = "1";
        var _lenCount = 0;
        var _ret = false;
        if (_strLen <= pLen / 2) {
            _cutString = pStr;
            _ret = true;
        }
        if (!_ret) {
            for (var i = 0; i < _strLen; i++) {
                if (this.isFull(pStr.charAt(i))) {
                    _lenCount += 2;
                } else {
                    _lenCount += 1;
                }

                if (_lenCount > pLen) {
                    _cutString = pStr.substring(0, i);
                    _ret = true;
                    break;
                } else if (_lenCount == pLen) {
                    _cutString = pStr.substring(0, i + 1);
                    _ret = true;
                    break;
                }
            }
        }
        if (!_ret) {
            _cutString = pStr;
            _ret = true;
        }

        if (_cutString.length == _strLen) {
            _cutFlag = "0";
        }
        return { "cutstring": _cutString, "cutflag": _cutFlag };
    }

    private static isFull(pChar) {
        for (var i = 0; i < pChar.strLen; i++) {
            if ((pChar.charCodeAt(i) > 128)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static autoAddEllipsis(pStr, pLen) {
        var _ret = this.cutString(pStr, pLen);
        var _cutFlag = _ret.cutflag;
        var _cutStringn = _ret.cutstring;

        if ("1" == _cutFlag) {
            return _cutStringn + "...";
        } else {
            return _cutStringn;
        }
    }

    public static getNumberChinese(num: number): string {
        let numStr = '';
        switch (num) {
            case 1: numStr = '壹'; break;
            case 2: numStr = '贰'; break;
            case 3: numStr = '叁'; break;
            case 4: numStr = '肆'; break;
            case 5: numStr = '伍'; break;
            case 6: numStr = '陆'; break;
            case 7: numStr = '柒'; break;
            case 8: numStr = '捌'; break;
            case 9: numStr = '玖'; break;
            case 0: numStr = '拾'; break;
            default: break;
        }
        return numStr;
    }

    public static getNumberCapital(num: number): string {
        let numStr = '';
        switch (num) {
            case 1: numStr = '一'; break;
            case 2: numStr = '二'; break;
            case 3: numStr = '三'; break;
            case 4: numStr = '四'; break;
            case 5: numStr = '五'; break;
            case 6: numStr = '六'; break;
            case 7: numStr = '七'; break;
            case 8: numStr = '八'; break;
            case 9: numStr = '九'; break;
            case 0: numStr = '十'; break;
            default: break;
        }
        return numStr;
    }

    public static getConvertFormatNumber(num: number, isCapital = true): string {
        let func;
        if (isCapital) {
            func = MKUtils.getNumberCapital;
        } else {
            func = MKUtils.getNumberChinese;
        }
        let highValue = Math.floor(num / 10);
        let lowValue = num % 10;
        let highStr = '';
        let lowStr = '';
        if (highValue > 0) {
            if (highValue === 1) {
                highStr = func(0);
            } else {
                highStr = func(highValue) + func(0);
            }
        }
        if (lowValue > 0) {
            lowStr = func(lowValue);
        }
        return (highStr + lowStr);
    }

    // 获取属性名
    public static getAttrName(num: number): string {
        let attrName = '';
        switch (num) {
            case 1: attrName = '生命'; break;
            case 2: attrName = '物攻'; break;
            case 3: attrName = '物防'; break;
            case 4: attrName = '法攻'; break;
            case 5: attrName = '法防'; break;
            case 6: attrName = '能量'; break;
            case 7: attrName = '暴击'; break;
            case 8: attrName = '暴伤'; break;
            case 9: attrName = '韧性'; break;
            case 10: attrName = '命中'; break;
            case 11: attrName = '闪避'; break;
            case 12: attrName = '治疗'; break;
            case 13: attrName = '先攻'; break;
            case 14: attrName = '物穿'; break;
            case 15: attrName = '法穿'; break;
            case 16: attrName = '控制'; break;
            default: break;
        }
        return attrName;
    }

    // 获取装备适用英雄的类型
    public static getEquipmentType(num: number): string {
        let attrName = '';
        switch (num) {
            case 1: attrName = '物理'; break;
            case 2: attrName = '法术'; break;
            case 3: attrName = '防御'; break;
            case 4: attrName = '治疗'; break;
            case 5: attrName = '控制'; break;
            default: break;
        }
        return attrName;
    }

    // 根据品质获取装备升级属性字段名
    public static getEquLvUpAttrName(quality: number): string {
        let attrName = 'White';
        switch (quality) {
            case 1: attrName = 'White'; break;
            case 2: attrName = 'Green'; break;
            case 3: attrName = 'Blue'; break;
            case 4: attrName = 'Purple'; break;
            case 5: attrName = 'Orange'; break;
            case 6: attrName = 'Red'; break;
            default: break;
        }
        return attrName;
    }

    // 通过品质获取对应的颜色值
    public static getColorByQuality(quality: number): cc.Color {
        let color = cc.color(255, 255, 255, 255);
        switch (quality) {
            case 1: color = cc.color(255, 255, 255, 255); break;
            case 2: color = cc.color(80, 214, 96, 255); break;
            case 3: color = cc.color(75, 167, 225, 255); break;
            case 4: color = cc.color(171, 89, 255, 255); break;
            case 5: color = cc.color(255, 147, 41, 255); break;
            case 6: color = cc.color(255, 92, 92, 255); break;

            default: break;
        }
        return color;
    }

    // 通过品质获得对应颜色16进制值
    public static getColorByQuality16(quality: number): string {
        let color;
        switch (quality) {
            case 1: color = cc.color(255, 255, 255, 255); break;
            case 2: color = cc.color(80, 214, 96, 255); break;
            case 3: color = cc.color(75, 167, 225, 255); break;
            case 4: color = cc.color(171, 89, 255, 255); break;
            case 5: color = cc.color(255, 147, 41, 255); break;
            case 6: color = cc.color(255, 92, 92, 255); break;

            default: break;
        }
        return color.toHEX("#rrggbb");
    }

    /**
     * 验证用户名是否含有特殊字符
     * @param str 
     */
    public static checkOtherChar(str: string) {
        var arr = ["&", "\\", "/", "*", ">", "<", "@", "!"];
        for (var i = 0; i < arr.length; i++) {
            for (var j = 0; j < str.length; j++) {
                if (arr[i] == str.charAt(j)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 移除两个数组里面的重复元素
     * @param arr1 
     * @param arr2 
     */
    public static removeDupItem(arr1, arr2) {
        var temp = []; //临时数组1 
        var temparray = [];//临时数组2 
        for (var i = 0; i < arr2.length; i++) {
            temp[arr2[i]] = true;//巧妙地方：把数组B的值当成临时数组1的键并赋值为真 
        };

        for (var i = 0; i < arr1.length; i++) {
            if (!temp[arr1[i]]) {
                //巧妙地方：同时把数组A的值当成临时数组1的键并判断是否为真，如果不为真说明没重复，就合并到一个新数组里，这样就可以得到一个全新并无重复的数组 
                temparray.push(arr1[i]);
            };
        };

        return temparray;
    }

    /**
     * 版本号比较
     * compareVersion('1.11.0', '1.9.9') return 1
     * @param v1 
     * @param v2 
     */
    public static compareVersion(v1, v2) {
        v1 = v1.split('.')
        v2 = v2.split('.')
        const len = Math.max(v1.length, v2.length)

        while (v1.length < len) {
            v1.push('0')
        }
        while (v2.length < len) {
            v2.push('0')
        }

        for (let i = 0; i < len; i++) {
            const num1 = parseInt(v1[i])
            const num2 = parseInt(v2[i])

            if (num1 > num2) {
                return 1
            } else if (num1 < num2) {
                return -1
            }
        }
        return 0
    }

}
