// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

var SpotSearchLightEnum = require('./SpotSearchLight_Enum')

var state = cc.Enum({
    idle: 0,
    moving: 1
})

cc.Class({
    extends: cc.Component,

    properties: {

        // lightSize: {
        //     default: cc.v2(263.0, 263.0),
        //     tooltip: "灯的大小"
        // },

        initLightPos: {
            default: cc.v2(-300, -300),
            tooltip: "灯光初始位置"
        },

        posWithAnimNode: {
            default: null,
            type: cc.Node,
            tooltip: "灯的运动轨迹的动画节点"
        },

        callbackHandler: {
            type: cc.Component.EventHandler,
            default: null,
            tooltip: "轨迹播放完成后的回调"
        },

        defaultAinm: {
            type: cc.Enum(SpotSearchLightEnum),
            default: SpotSearchLightEnum.划过,
            tooltip: "默认轨迹"
        }

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        // 获取光源图
        let tx = this.posWithAnimNode.getComponent(cc.Sprite).spriteFrame._texture
        let width = this.posWithAnimNode.width * this.posWithAnimNode.scale
        let height = this.posWithAnimNode.height * this.posWithAnimNode.scale

        // 更新widget适配
        let widget = this.node.getComponent(cc.Widget)
        widget.updateAlignment()

        // 整理shader所需数据
        this._material = this.node.getComponent(cc.RenderComponent).getMaterial(0)
        this._material.setProperty("maskSize", cc.v2(this.node.width, this.node.height))
        this._material.setProperty("lightTexture", tx)
        this._material.setProperty("lightSize", cc.v2(width, height))
        this._material.setProperty("lightPos", this.initLightPos)
        this._material.setProperty("lightVisible", 0)

        // 设置状态
        this._state = state.idle
        this._moveAnimIndex = this.defaultAinm
    },

    // 按类型显示效果最后一帧
    ShowEnd(index) {
        index = (index == null || index == undefined) ? this.defaultAinm : index
        this._state = state.idle
        if (index == SpotSearchLightEnum.关闭) {// 关闭
            this._anim.stop()
            this.node.active = false
            this._material.setProperty("lightPos", this.initLightPos)
            this._material.setProperty("lightVisible", 0)
            return
        } else if (index == SpotSearchLightEnum.无灯) {// 无灯遮罩
            this._state = state.idle
            this.node.active = true
            this._material.setProperty("lightPos", this.initLightPos)
            this._material.setProperty("lightVisible", 0)
            return
        }

        this._moveAnimIndex = index
        this.node.active = true

        let clips = this._anim.getClips()
        let clip = clips[this._moveAnimIndex]
        this._anim.play(clip.name, clip.duration)

        this.callbackHandler = null
        //如果切换过快,可能会出现上一次注册的finish回调在此showend后触发,将state还原,所以需要在此处拿掉finish
        this._anim.off('finished', this._onAnimFinish, this)
        //因为拿掉了finished监听,做个标记,到update里重置state
        this._monitifyState = true

        this._material.setProperty("lightPos", this.initLightPos)
        this._material.setProperty("lightVisible", 1)

        this._state = state.moving
    },

    // 按类型显示聚光灯效果
    Show(index, cbHandler) {
        index = (index == null || index == undefined) ? this.defaultAinm : index

        //场景里会默认隐藏,先触发此方法active节点,故在此处获取.
        this._anim = this.posWithAnimNode.getComponent(cc.Animation)

        if (index == -2) {// 关闭
            this._state = state.idle
            this._anim.stop()
            this.node.active = false
            this._material.setProperty("lightPos", this.initLightPos)
            this._material.setProperty("lightVisible", 0)
            return
        } else if (index == -1) {// 无灯遮罩
            this._state = state.idle
            this.node.active = true
            this._material.setProperty("lightPos", this.initLightPos)
            this._material.setProperty("lightVisible", 0)
            return
        }

        this._moveAnimIndex = index
        this.node.active = true

        let clips = this._anim.getClips()
        let clip = clips[this._moveAnimIndex]
        this._anim.play(clip.name)

        this.callbackHandler = cbHandler
        this._anim.on('finished', this._onAnimFinish, this)

        //如果show在另一个showEnd之后触发,需要将状态修正标记取消,以保证show的正常执行
        this._monitifyState = false

        this._material.setProperty("lightPos", this.initLightPos)
        this._material.setProperty("lightVisible", 1)
        this._state = state.moving
    },

    _onAnimFinish() {
        this._state = state.idle
        if (this.callbackHandler) {
            this.callbackHandler.emit([this._moveAnimIndex])
        }
        this._anim.off('finished', this._onAnimFinish, this)
    },

    // 跟随动画的行为,放到late里执行
    lateUpdate(dt) {
        if (this._state == state.moving) {
            let width = this.posWithAnimNode.width * this.posWithAnimNode.scale
            let height = this.posWithAnimNode.height * this.posWithAnimNode.scale
            this._material.setProperty("lightSize", cc.v2(width, height))
            this._material.setProperty("lightPos", cc.v2(this.node.width / 2 + this.posWithAnimNode.x - width / 2, this.node.height / 2 + this.posWithAnimNode.y - height / 2))
        }
        if (!!this._monitifyState) {
            this._state = state.idle
            this._monitifyState = false
        }
    },

    onDestroy() {
    },

})
