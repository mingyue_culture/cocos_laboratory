import { isPlatWechatGame } from "./MKSystem";
import WxApi from "./WXApi";

/**
 * SDK 封装
 * Author: lingfeng
 * Date: 2019.1.4
 */

const {ccclass, property} = cc._decorator;

@ccclass
export default class MKSDK extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    onLoad () {}

    start () {

    }

    // update (dt) {}

    public static getNetworkType(callback: Function){
        if(isPlatWechatGame()){
            WxApi.getNetworkType(callback);
        }
    }
}
