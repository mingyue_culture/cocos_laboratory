
/**
 * Author: ling_feng
 * Date: 2018.5.17
 * CopyRight:
 * 数据计算类
 */

export default class MKMath{
    /**
    * 区间随机数
    */
   public static getRandom(min, max) {
        return min + Math.round(Math.random() * (max - min));
    }

    /**
     * 判断一个点是否在矩形内部
     * @param point 
     * @param vec4 
     */
    public static isPointInVector(point: cc.Vec2, vec4: cc.Rect) {
        return point.x >= vec4.x && point.x <= (vec4.x + vec4.width) && point.y >= vec4.y && point.y <= (vec4.y + vec4.height);
    }

    /**
     * 计算一个点相对于另一个点的角度
     * @param p1 
     * @param p2 
     */
    public static getAngleOfTowPoint(p1: cc.Vec2, p2: cc.Vec2) {
        //return -cc.pToAngle(cc.pSub(p1, p2)) / Math.PI * 180;;

        let atkDir = p1.sub(p2);
        let angle = cc.v2(1, 0).signAngle(atkDir);
        let deg = cc.misc.radiansToDegrees(angle);
        return (180 - deg);
    }

    /**
     * 判断位移
     * @param p1 
     * @param p2 
     */
    public static isPosChange(p1: number, p2: number) {
        if (Math.abs(p1 * 10000 - p2 * 10000) > 8)
            return true;
        else
            return false;
    }

    /**
     * 随机字符串
     * @param len 
     */
    public static randomString(len) {
        len = len || 32;
        var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';    /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
        var maxPos = $chars.length;
        var pwd = '';
        for (let i = 0; i < len; i++) {
            pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
        }
        return pwd;
    }

    private static toNum(a) {
        var a = a.toString();
        //也可以这样写 var c=a.split(/\./);
        var c = a.split('.');
        var num_place = ["", "0", "00", "000", "0000"], r = num_place.reverse();
        for (var i = 0; i < c.length; i++) {
            var len = c[i].length;
            c[i] = r[len] + c[i];
        }
        var res = c.join('');
        return res;
    }

}
