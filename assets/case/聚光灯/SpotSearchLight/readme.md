#聚光灯空控件

###原理
shader实现在遮罩上显示透明形状,作为独立组件使用,场景其他显示元素不可放置于组件子级.
透明形状由固定子节点pos上的spriteframe决定
运行轨迹由固定子节点pos上的动画控制
在组件的lateupdate中更新shader所需灯光大小和位置

###可配置参数构成:
1. 灯光运行轨迹组件 -- posWithAnim
    - 用来播放轨迹动画
2. 灯光图片 -- 灯光运行轨迹组件-Sprite组件-SpriteFrame
    - 只使用该图片形状和透明度分布
3. 灯光初始化位置 -- initLightPos
    - 灯光第一帧出现的位置 
4. 默认轨迹动画 -- defaultAnim
可选参数:
轨迹动画完成后的回调 -- callbakHandler 该参数逻辑层面可能会动态赋值
 