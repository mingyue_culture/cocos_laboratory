
const { ccclass, property } = cc._decorator;

@ccclass
export default class BgComponent extends cc.Component {

    @property({ type: cc.Node, tooltip: "背景图1" })
    bg1: cc.Node = null;

    @property({ type: cc.Node, tooltip: "背景图2" })
    bg2: cc.Node = null;

    private speed: number = 0;

    onLoad() {

    }

    start() {
        this.setSpeed(200);
    }

    setSpeed(speed: number) {
        this.speed = speed;
    }

    update(dt) {
        // 同时移动两个背景
        this.bg1.y -= this.speed * dt;
        this.bg2.y -= this.speed * dt;

        let bgHeight = this.bg2.height;
        let winHight = cc.winSize.height;

        /**
         * 背景图移动到边界值 重置坐标
         * bgHeight + this.bg2.y 是关键 bg1背景图离开屏幕的时候 位置可能是cc.v2(0,-1282)
         * 此时bg2的位置是cc.v2(0, -2) 所以背景图的高度加上bg2的位置 才能完整的拼合两张图
         */
        if (this.bg1.y <= -winHight) {
            this.bg1.y = bgHeight + this.bg2.y;
        }

        /**
         * 同理
         */
        if (this.bg2.y <= -winHight) {
            this.bg2.y = bgHeight + this.bg1.y;
        }

        // 事情从简单做 日积月累 终可成就
    }
}