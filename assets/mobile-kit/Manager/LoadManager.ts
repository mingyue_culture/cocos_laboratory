/**
 * 加载管理器
 * Author: lingfeng
 * Date: 2018.12.23
 */

const {ccclass, property} = cc._decorator;

@ccclass
export default class LoadManager extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    // LIFE-CYCLE CALLBACKS:

    onLoad () {

    }

    start () {

    }

    // update (dt) {}
}
