/**
 * UI管理类
 */

export default class MKUIManager {
    private UILayerContainer: Array<any> = [];


    public constructor() {

    }

    private static ins: MKUIManager;
    public static get I(): MKUIManager {
        if (!this.ins) {
            this.ins = new MKUIManager();
        }
        return this.ins;
    }

    public getUIContainer() {
        return this.UILayerContainer;
    }

    public put(func: Function) {
        this.UILayerContainer.push(func);
    }

    public reCallback() {
        let func = this.UILayerContainer.shift();
        func && func();
    }

    /**
     * 设置UI打开路径
     */
    setUISource(func: Function) {

    }

    /**
     * 回调UI
     */

}