import { isPlatWechatGame, isPlatTouTiaoGame } from "./MKSystem";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MKDataReport extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

    }

    start() {

    }

    /*********************************** 阿拉丁日志 *********************************/

    public static aladdinEvent(key: any = "default", param: any = {}) {
        if (!isPlatWechatGame() || !isPlatTouTiaoGame()) {
            return;
        }

        window['wx'].aldSendEvent(key, param);
    }
}
