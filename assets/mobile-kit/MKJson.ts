
export default class MKJson{
    private m_jsonObject:any = null;

    public constructor (str = undefined) {
        if (str != undefined) {
            this.parseString(str);
        }
    }

    parseString (str) {
        try {
            this.m_jsonObject = JSON.parse(str);
        }
        catch (e) {
            this.m_jsonObject = null;
        }
    }

    parseFile (file) {
        var fileData = cc.loader.getRes(file);
        if (!fileData) {
            //cc.log("Please load the resource first : " + file);
            return;
        }

        this.m_jsonObject = fileData;
    }

    setJsonObject(data) {
        this.m_jsonObject = data;
    }

    getJsonObject() {
        return this.m_jsonObject;
    }

    getStringValue (key) {
        if (this.isObject()) {
            var value = this.m_jsonObject[key];
            if (value != undefined) {
                return value;
            }
            else {
                return "";
            }
        }
        else {
            return "";
        }
    }

    getNumberValue (key, def) {
        if(def == undefined){
            def = 0;
        }
        if (this.isObject()) {
            var value = this.m_jsonObject[key];
            if (value != undefined) {
                return value;
            }
            else {
                return def;
            }
        }
        else {
            return def;
        }
    }

    getBoolValue (key) {
        if (this.isObject()) {
            var value = this.m_jsonObject[key];
            if (value != undefined) {
                return value;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    getObjectValue (key) {
        if (this.isObject()) {
            var value = this.m_jsonObject[key];
            if (value != undefined) {
                var jsonValue = new MKJson();
                jsonValue.m_jsonObject = value;
                return jsonValue;
            }
            else {
                return new MKJson();
            }
        }
        else {
            return new MKJson();
        }
    }

    getSize () {
        if (this.isObject()) {
            var value = this.m_jsonObject["length"];
            if (value != undefined) {
                return this.m_jsonObject.length;
            }
            else {
                return 0;
            }
        }
        else {
            return 0;
        }
    }

    getArrayElement (idx) {
        if (this.isObject()) {
            if (this.m_jsonObject.length != undefined && idx < this.m_jsonObject.length) {
                var value = this.m_jsonObject[idx];
                var jsonValue = new MKJson();
                jsonValue.m_jsonObject = value;
                return jsonValue;
            }
            else {
                return new MKJson();
            }
        }
        else {
            return new MKJson();
        }
    }

    isObject () {
        return this.m_jsonObject != null && this.m_jsonObject != undefined;
    }
}
