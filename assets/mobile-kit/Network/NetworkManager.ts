/**
 * Author: ling_feng
 * Date: 2018.5.23
 * CopyRight:
 * 通信管理类
 */

import MKSocket from './MKSocket'

/**
 * 端口号
 */
var Server_Port_Common = "";

export default class NetworkManager {
    private m_commonSocket: MKSocket = null;

    public constructor() {
        this.m_commonSocket = new MKSocket();
    }

    public static ins: NetworkManager;
    public static get I(): NetworkManager {
        if (!this.ins) {
            this.ins = new NetworkManager();
        }
        return this.ins;
    }

    public getComSocket(): MKSocket {
        return this.m_commonSocket;
    }
}